import api from "../api";
import Axios from "axios";
import { store } from "../../pages/index";
import { setAppLoading } from "../../store/actions/app";

export const getCep = async cep => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    Axios.get(
      `http://webservice.uni5.net/web_cep.php?auth=bf570b51d02f11a50c61d6d54322565d&formato=json&cep=${cep}`
    ).then(({ data }) => {
      const buscaCep = data;
      if (data.logradouro === "") {
        resolve({ error: "" });
      } else {
        api
          .get(`cities/search/${buscaCep.cidade}/${buscaCep.uf}`)
          .then(({ data }) => {
            const res = {
              ...buscaCep,
              city_id: data.id
            };
            resolve(res);
            store.dispatch(setAppLoading(false));
          });
      }
    });
  });
};
export const uploadPhoto = async (photo, id) => {
  return new Promise(function(resolve, reject) {
    api.post(`advertisings/${id}/uploadPhoto`, photo).then(({ data }) => {
      resolve(data);
    });
  });
};
