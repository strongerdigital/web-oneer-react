import api from "../api";
import { store } from "../../pages/layouts/main";
import { setAppLoading } from "../../store/actions/app";

class Data {
  async featured() {
    const response = await api.get("/featured");
    return response.data;
  }
  async categories() {
    const response = await api.get("/categories");
    return response.data;
  }
  async category(id) {
    const response = await api.get("/categories/" + id);
    return response.data;
  }
  async place(id) {
    const response = await api.get("/advertisings/" + id);
    return response.data;
  }
  async myplace(id) {
    const response = await api.get("/advertisings/" + id);
    return response.data;
  }
  async search(term) {
    const response = await api.post("/advertisings/search", { term });
    return response.data;
  }
  async featured() {
    const response = await api.get("/advertisings/featured/list");
    return response.data;
  }
  async last4() {
    const response = await api.get("/advertisings/last4/list");
    return response.data;
  }
  async accessories() {
    const response = await api.get("/accessories");
    return response.data;
  }
  async countries() {
    const response = await api.get("/countries");
    return response.data;
  }
  async getLocation(id) {
    const response = await api.get("/locations/" + id);
    return response.data;
  }
}
const Places = new Data();
export default Places;
