import api from "../api";
import { store } from "../../pages/index";
import { setAppLoading } from "../../store/actions/app";

export const makeLogin = data => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.post(`auth/signin`, data).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};

export const makeSignup = data => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.post(`auth/signup`, data).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const updateUser = data => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.put(`users/${data.id}`, data).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const getLogged = async () => {
  const logged = await localStorage.getItem("@oneer:user");
  if (logged) {
    return JSON.parse(logged);
  } else {
    return {};
  }
};
export const savePlace = async data => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.post(`advertisings/`, data).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const getMyPlaces = async id => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`users/${id}/advertisings/`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const getMyLocations = async id => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`users/${id}/locations/`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const confirmationLocation = async (data, user) => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api
      .post(`users/${user.id}/confirmation_location/`, data)
      .then(({ data }) => {
        resolve(data);
        store.dispatch(setAppLoading(false));
      });
  });
};
export const makePayment = async (data, user) => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.post(`users/${user.id}/makepayment/`, data).then(({ data }) => {
      setTimeout(() => {
        resolve(data);
        store.dispatch(setAppLoading(false));
      }, 1500);
    });
  });
};
