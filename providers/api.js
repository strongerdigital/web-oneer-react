import axios from "axios";
import env from "../consts/env";

const api = axios.create({
  baseURL: env.api
});
export default api;
