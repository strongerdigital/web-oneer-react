import createNumberMask from "text-mask-addons/dist/createNumberMask";

import { format } from "date-fns";
export const areaMask = createNumberMask({
  allowDecimal: true,
  decimalSymbol: ".",
  prefix: "",
  includeThousandsSeparator: false,
  decimalLimit: 2
  // requireDecimal: true
});
export const moneyMask = createNumberMask({
  allowDecimal: true,
  decimalSymbol: ",",
  prefix: "",
  includeThousandsSeparator: false,
  thousandsSeparatorSymbol: ".",
  decimalLimit: 2
  // requireDecimal: true
});

export const phoneMask = [
  "(",
  /[1-9]/,
  /\d/,
  ")",
  " ",
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  "-",
  /\d/,
  /\d/,
  /\d/,
  /\d/
];
export const mobileMask = [
  "(",
  /[1-9]/,
  /\d/,
  ")",
  " ",
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  "-",
  /\d/,
  /\d/,
  /\d/,
  /\d/
];
export const dateMask = [
  /[0-9]/,
  /\d/,
  "/",
  /\d/,
  /\d/,
  "/",
  /\d/,
  /\d/,
  /\d/,
  /\d/
];
export const cepMask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/];
export const cpfMask = [
  /[0-9]/,
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  ".",
  /\d/,
  /\d/,
  /\d/,
  "-",
  /\d/,
  /\d/
];
export const cvcMask = [/[0-9]/, /\d/, /\d/];
export const cardMask = [
  /[0-9]/,
  /\d/,
  /\d/,
  /\d/,
  " ",
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  " ",
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  " ",
  /\d/,
  /\d/,
  /\d/,
  /\d/
];

export const formatDateHour = date => {
  let d = "Não informado";
  let formatedDate = date.split("T");
  if (formatedDate[1]) {
    const t = formatedDate[0] + " " + formatedDate[1];
    d = format(new Date(t), "dd/MM/yyyy HH:mm");
  }
  return d;
};
