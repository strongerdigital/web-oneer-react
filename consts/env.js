const online = true;

const url = online
  ? "https://oneer.app:21014"
  : "http://127.0.0.1:21014";

const env = {
  publicURL: `${url}/`,
  filesURL: `${url}/uploads/`,
  api: `${url}/api/`
};

export default env;
