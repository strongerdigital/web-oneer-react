let defaultState = {
  loading: false,
  mininav: false,
  logged: null
};

const rootReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "LOADING":
      return {
        ...state,
        loading: action.loading
      };
    case "LOGGED":
      return {
        ...state,
        logged: action.logged
      };
    case "SET_MININAV":
      return {
        ...state,
        mininav: state.mininav ? false : true
      };
    default:
      return state;
  }
};

export default rootReducer;
