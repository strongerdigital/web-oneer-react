export function setAppLoading(loading) {
  return dispatch => {
    if (loading === true) {
      dispatch(setLoading(loading));
    } else {
      setTimeout(() => {
        dispatch(setLoading(loading));
      }, 500);
    }
  };
}
export function setLogged(logged) {
  return dispatch => {
    dispatch(setLoggedUser(logged));
  };
}
export function handleMiniNavbar() {
  return dispatch => {
    dispatch(setMiniNavbar());
  };
}

export function setLoading(loading) {
  return {
    type: "LOADING",
    loading
  };
}
export function setMiniNavbar() {
  return {
    type: "SET_MININAV"
  };
}
export function setLoggedUser(logged) {
  return {
    type: "LOGGED",
    logged
  };
}
