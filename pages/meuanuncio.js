import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const MyPlace = ({ data }) => <Main data={data} />;

MyPlace.getInitialProps = async ({ query }) => {
  return {
    data: {
      myplace: await Loader.myplace(query.id),
      page: "myplace"
    }
  };
};

export default MyPlace;
