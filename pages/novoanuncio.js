import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const NewPlace = ({ data }) => <Main data={data} />;

NewPlace.getInitialProps = async () => {
  return {
    data: {
      accessories: await Loader.accessories(),
      categories: await Loader.categories(),
      countries: await Loader.countries(),
      page: "newPlace"
    }
  };
};

export default NewPlace;
