import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const Categories = ({ data }) => <Main data={data} />;

Categories.getInitialProps = async () => {
  return {
    data: {
      categories: await Loader.categories(),
      page: "categories"
    }
  };
};

export default Categories;
