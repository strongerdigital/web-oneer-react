import React from "react";
import Main from "./layouts/main";

const UpdateProfile = ({ data }) => <Main data={data} />;

UpdateProfile.getInitialProps = async ({ query }) => {
  return {
    data: {
      page: "updateProfile"
    }
  };
};

export default UpdateProfile;
