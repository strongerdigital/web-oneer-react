import React from "react";
import Main from "./layouts/main";

const MyLocations = ({ data }) => <Main data={data} />;

MyLocations.getInitialProps = async () => {
  return {
    data: {
      page: "myLocations"
    }
  };
};

export default MyLocations;
