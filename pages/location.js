import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const Location = ({ data }) => <Main data={data} />;

Location.getInitialProps = async ({ query }) => {
  return {
    data: {
      location: await Loader.getLocation(query.advertising_location),
      page: "location"
    }
  };
};

export default Location;
