import React from "react";
import Main from "./layouts/main";

const Locations = ({ data }) => <Main data={data} />;

Locations.getInitialProps = async () => {
  return {
    data: {
      page: "locations"
    }
  };
};

export default Locations;
