import React from "react";
import Main from "./layouts/main";

const MyFavorites = ({ data }) => <Main data={data} />;

MyFavorites.getInitialProps = async () => {
  return {
    data: {
      page: "myFavorites"
    }
  };
};

export default MyFavorites;
