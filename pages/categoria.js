import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const Category = ({ data }) => <Main data={data} />;

Category.getInitialProps = async ({ query }) => {
  return {
    data: {
      category: await Loader.category(query.slug),
      accessories: await Loader.accessories(),
      page: "category"
    }
  };
};

export default Category;
