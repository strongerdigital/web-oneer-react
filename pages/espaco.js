import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const Place = ({ data }) => <Main data={data} />;

Place.getInitialProps = async ({ query }) => {
  return {
    data: {
      place: await Loader.place(query.slug),
      page: "place"
    }
  };
};

export default Place;
