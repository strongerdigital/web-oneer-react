import React from "react";
import Main from "./layouts/main";

const Messages = ({ data }) => <Main data={data} />;

Messages.getInitialProps = async () => {
  return {
    data: {
      page: "messages"
    }
  };
};

export default Messages;
