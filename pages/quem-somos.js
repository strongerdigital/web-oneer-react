import React from "react";
import Main from "./layouts/main";

const About = ({ data }) => <Main data={data} />;

About.getInitialProps = async () => {
  return {
    data: {
      page: "about"
    }
  };
};

export default About;
