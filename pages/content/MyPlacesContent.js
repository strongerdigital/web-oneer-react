import React, { useEffect, useState } from "react";
import Title from "./components/Title";
import { connect } from "react-redux";
import { getLogged, getMyPlaces } from "../../providers/services/user";
import * as AppActions from "../../store/actions/app";
import ProfileActions from "./components/ProfileActions";
import ProfileMenu from "./components/ProfileMenu";
import MyPlacesContent from "./components/MyPlacesContent";
// import { Container } from './styles';

function MyPlacesPrincipal() {
  const [places, setPlaces] = useState([]);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        getMyPlaces(user.id).then(items => {
          setPlaces(items);
        });
      }
    });
  }, []);
  return (
    <>
      <div className="clearfix"></div>
      <Title title={"Meus Anúncios"}></Title>
      <div className="container">
        <div className="row sticky-wrapper">
          <ProfileActions></ProfileActions>
          <div className="row">
            <ProfileMenu></ProfileMenu>
            <div className="col-md-9">
              <MyPlacesContent places={places}></MyPlacesContent>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default MyPlacesPrincipal;
