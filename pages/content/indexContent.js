import React from "react";
import Banner from "./components/Banner";
import Categories from "./components/Categories";
import Featured from "./components/Featured";
import CTAText from "./components/CTAText";
import CTAActions from "./components/CTAActions";
import CTAIcons from "./components/CTAIcons";

const IndexPrincipal = ({ data }) => (
  <>
    <div className="clearfix"></div>
    <Banner />
    <Categories data={data && data.categories} />
    <Featured places={data && data.last4} />
    <CTAText />
    <CTAActions />
    <CTAIcons />
  </>
);

export default IndexPrincipal;
