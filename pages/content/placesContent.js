import React from "react";
import Title from "./components/Title";
import Results from "./components/Results";
import Filter from "./components/Filter";

const PlacesContentPrincipal = ({ data }) => (
  <>
    <div className="clearfix"></div>
    <Title title={"Resultados para a sua busca"}></Title>
    <div className="container">
      <div className="row sticky-wrapper">
        <Results data={data && data.places}></Results>
        {data && data.places && data.places.length > 0 && <Filter data={data}></Filter>}
      </div>
    </div>
  </>
);

export default PlacesContentPrincipal;
