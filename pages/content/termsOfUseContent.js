import React from "react";
import Title from "./components/Title";
import Results from "./components/Results";
import Filter from "./components/Filter";

const TermsOfUsePrincipal = props => (
  <>
    <div className="clearfix"></div>
    <Title title={"Termos de Uso e Política de Privacidade"}></Title>
    <div className="content-box">
      <div className="container">
        <p>
          Estes Termos de Serviço (“Termos”) constituem um contrato
          vinculante(“Contrato”) entre você e a Oneer (conforme definido
          abaixo), regendo seu acesso ao site da Oneer e seu uso, incluindo seus
          subdomínios, e quaisquer outros sites nos quais a Oneer disponibilize
          seus serviços (coletivamente, “Site”), nossos aplicativos de celular,
          tablet ou de outro dispositivo smart aplicável, e interfaces de
          programa do aplicativo (coletivamente, “Aplicativo”) e todos os
          serviços relacionados (coletivamente, “Serviços da Oneer”).
        </p>
        <p>
          O Site, o Aplicativo e os Serviços da Oneer serão coletivamente
          referidos como a “Plataforma Oneer”. Nossos Termos de Garantia ao
          Locação, nossa Política de Reembolso do Locatário, nossa Política de
          Não Discriminação e outras Políticas aplicáveis ao seu uso da
          Plataforma Oneer estão incorporados por referência neste Contrato.
        </p>
        <p>
          Quando estes Termos mencionarem “Oneer”, “nós”, “nos”, “nosso”, e
          outras variações gramaticais da primeira pessoa do plural, eles se
          referem à empresa Oneer com a qual você está firmando um contrato.
        </p>
        <p>
          Você está firmando contrato com a Oneer Intermediações e Serviços
          Digitais Ltda., inscrita no CNPJ nº 33.184.323/0001-31, localizada à
          Rua Doutor Eurico Cesar de Almeida, nº 277, Bairro Bacacheri, CEP
          82.510-400, na cidade de Curitiba – Estado do Paraná. Consta descrição
          da nossa coleta e de nosso uso de informações pessoais relacionadas ao
          seu acesso e uso da Plataforma Oneer em nossa Política de Privacidade.
        </p>
        <p>
          Todo e qualquer serviço de processamento de pagamento através de ou
          relacionado ao seu uso da Plataforma Oneer (“Serviços de Pagamento”)
          são prestados a você por uma ou mais entidades, conforme estabelecido
          nos Termos de Pagamento de Serviços (“Termos de Pagamento”).
        </p>
        <br />
        <p>
          <strong>Índice</strong>
        </p>
        <br />
        <p>
          <a title="Escopo dos Serviços da Oneer" href="#Escopo">
            1. Escopo dos Serviços da Oneer
          </a>
        </p>
        <p>
          <a
            title="Elegibilidade, Utilização da Plataforma Oneer"
            href="#Elegibilidade"
          >
            2. Elegibilidade, Utilização da Plataforma Oneer, Verificação de
            Membro
          </a>
        </p>
        <p>
          <a title="Alteração destes Termos" href="#Alteracao">
            3. Alteração destes Termos
          </a>
        </p>
        <p>
          <a title="Registro de Conta" href="#Registro">
            4. Registro de Conta
          </a>
        </p>
        <p>
          <a title="Conteúdo" href="#Conteúdo">
            5. Conteúdo
          </a>
        </p>
        <p>
          <a title="Taxas de Serviço" href="#Taxas">
            6. Taxas de Serviço
          </a>
        </p>
        <p>
          <a title="Termos específicos para Usuários" href="#TermosUsuarios">
            7. Termos específicos para Usuários
          </a>
        </p>
        <p>
          <a
            title="Termos específicos para Locatários"
            href="#TermosLocatarios"
          >
            8. Termos específicos para Locatários
          </a>
        </p>
        <p>
          <a
            title="Alterações, Cancelamentos e Reembolsos de Reserva"
            href="#Alteracoes"
          >
            9. Alterações, Cancelamentos e Reembolsos de Reserva, Central de
            Resoluções
          </a>
        </p>
        <p>
          <a title="Avaliações e Comentários" href="#Avaliacoes">
            10. Avaliações e Comentários
          </a>
        </p>
        <p>
          <a title="Danos às Acomodações, Litígios entre Membros" href="#Danos">
            11. Danos às Acomodações, Litígios entre Membros
          </a>
        </p>
        <p>
          <a title="Arredondamento, Conversão de moeda" href="#Arredondamento">
            12. Arredondamento, Conversão de moeda
          </a>
        </p>
        <p>
          <a title="Impostos" href="#Impostos">
            13. Impostos
          </a>
        </p>
        <p>
          <a title="Atividades Proibidas" href="#Atividades">
            14. Atividades Proibidas
          </a>
        </p>
        <p>
          <a title="Prazo e Rescisão, Suspensão e outras Medidas" href="#Prazo">
            15. Prazo e Rescisão, Suspensão e outras Medidas
          </a>
        </p>
        <p>
          <a title="Isenções" href="#Isencoes">
            16. Isenções
          </a>
        </p>
        <p>
          <a title="Responsabilidade" href="#Responsabilidade">
            17. Responsabilidade
          </a>
        </p>
        <p>
          <a title="Indenização" href="#Indenizacao">
            18. Indenização
          </a>
        </p>
        <p>
          <a title="Resolução de Litígios" href="#ResolucaoLitigios">
            19. Resolução de Litígios
          </a>
        </p>
        <p>
          <a title="Feedback" href="#Feedback">
            20. Feedback
          </a>
        </p>
        <p>
          <a title="Lei Aplicável e Jurisdição" href="#Lei">
            21. Lei Aplicável e Jurisdição
          </a>
        </p>
        <p>
          <a title="Disposições Gerais" href="#Disposições">
            22. Disposições Gerais
          </a>
        </p>
        <br />
        <p>
          <strong id="Escopo">1. Escopo dos Serviços da Oneer</strong>
        </p>
        <p>
          1.1. A Plataforma Oneer é um mercado on-line que permite aos usuários
          cadastrados (“Membros”), e terceiros determinados, que oferecem
          serviços de locação, comunicar-se e fazer transações diretas com
          membros que estejam buscando locação de imóveis comerciais por
          períodos determinados. Os Serviços podem incluir a oferta de
          escritórios, consultórios, estúdios para atividade indoor, salas de
          reuniões, salas de videoconferência, salas comerciais, escritórios
          e/ou consultórios corporativos, coworkings, espaços físicos para
          eventos, entre outros, para atividades por períodos determinados com
          características eminentemente comerciais, sendo vedada a locação
          residencial.
        </p>
        <p>
          1.2. Como o fornecedor da Plataforma, a Oneer não é proprietária, não
          cria, vende, revende, fornece, controla, gerencia, oferece, entrega ou
          abastece qualquer Anúncio ou Serviços de Locação. Os Usuários são os
          únicos responsáveis por seus Anúncios e Serviços de Locação. Quando os
          membros fazem ou aceitam uma reserva, eles celebram um contrato
          diretamente um com o outro. A Oneer não é e não se torna parte ou
          outro participante de qualquer relacionamento contratual entre os
          Membros, tampouco a Oneer é uma corretora de imóveis ou seguradora. A
          Oneer não atua como um agente em qualquer capacidade para um Membro,
          exceto conforme especificado nos Termos de Pagamento.
        </p>
        <p>
          1.3. Embora possamos ajudar a facilitar a resolução de disputas, a
          Oneer não tem qualquer controle sobre e não garante: (i) a existência,
          qualidade, segurança, sustentabilidade ou licitude de qualquer Anúncio
          ou Serviços de Locação; (ii) a veracidade e a precisão de quaisquer
          descrições de Anúncio, Avaliações, Comentários, ou outros Conteúdos de
          um Membro; ou, (iii) o desempenho ou a conduta de qualquer Membro ou
          terceiro. A Oneer não endossa qualquer Membro, Anúncio ou Serviços de
          Locação. Quaisquer referências a um Membro sendo “verificado” (ou
          linguagem similar) apenas indica que o Membro completou um processo de
          verificação relevante, e nada a mais. Nenhuma dessas descrições
          significa endosso, certificação ou garantia fornecidos pela Oneer
          sobre o Membro, inclusive sobre a identidade ou o histórico do Membro
          ou se o Membro é confiável, seguro ou adequado. Você deve sempre tomar
          os devidos cuidados ao utilizar os Serviços de Locação,
          independentemente de ser online ou pessoalmente. As imagens
          verificadas (conforme definido abaixo) visam apenas a indicar uma
          representação fotográfica de um Anúncio no momento em que a fotografia
          foi tirada, e; portanto, não representam nenhum endosso por parte da
          Oneer de qualquer Locação ou Anúncio.
        </p>
        <p>
          1.4. Ao optar por utilizar a Plataforma Oneer para ofertar imóveis
          comerciais à Locação (conforme definido abaixo), seu relacionamento
          com a Oneer está limitado a ser um membro e um contratado terceiro,
          independente e não um funcionário, agente, associado ou parceiro da
          Oneer por qualquer motivo, e que você atuará exclusivamente em seu
          próprio nome e em seu benefício próprio, e não em nome ou em benefício
          da Oneer. A Oneer, de modo geral, não direciona ou controla você ou
          seu desempenho sobre estes Termos especificamente, incluindo em
          conexão com sua provisão dos Serviços de Locação. Você reconhece e
          concorda que tem total descrição para anunciar Serviços de Locação ou
          de alguma forma se envolver em outras atividades comerciais.
        </p>
        <p>
          1.5. Para promover a Plataforma Oneer e ampliar a exposição dos
          Anúncios para possíveis interessados podem ser divulgados em outros
          sites, em aplicativos, em e-mails, e em anúncios on-line e off-line.
          Para auxiliar os Membros falantes de idiomas diferentes, podem ser
          traduzidos o conteúdo dos Anúncios ou outros conteúdos de Membros,
          total ou parcialmente, para outros idiomas. A Oneer não pode garantir
          a precisão ou a qualidade dessas traduções, e os Membros são
          responsáveis por revisar e verificar a precisão das referidas
          traduções. A Plataforma Oneer pode conter traduções fornecidas pelo
          Google. O Google renuncia a todas as garantias relacionadas às
          traduções, expressas ou implícitas, incluindo quaisquer garantias de
          exatidão, confiabilidade, e quaisquer garantias implícitas de
          comercialidade ou de adequação para um propósito específico e de não
          violação.
        </p>
        <p>
          1.6. A Plataforma Oneer pode conter links para sites ou recursos de
          terceiros (“Serviços de Terceiros”). Esses Serviços de Terceiros podem
          estar sujeitos a termos e condições e práticas de privacidade
          diferentes. A Oneer não é responsável pela disponibilidade ou precisão
          dos referidos Serviços de Terceiros, tampouco pelo conteúdo, pelos
          produtos ou serviços disponíveis nesses Serviços de Terceiros. Os
          Links para esses Serviços de Terceiros não são um endosso da Oneer
          para os referidos Serviços de Terceiros.
        </p>
        <p>
          1.7. Devido à natureza da Internet, a Oneer não pode garantir uma
          disponibilidade e acessibilidade ininterruptas e contínuas da
          Plataforma Oneer. A Oneer pode restringir a disponibilidade da
          Plataforma Oneer ou de certas áreas ou recursos a ela relacionados,
          caso seja necessário considerando os limites de capacidade, a
          segurança ou a integridade de nossos servidores, ou para realizar
          medidas de manutenção que garantam o funcionamento devido ou melhorado
          da Plataforma Oneer. A Oneer pode melhorar e alterar a Plataforma
          Oneer e introduzir novos Serviços da Oneer de tempos em tempos.
        </p>
        <p>
          1.8. Comunicação com o Usuário. O Usuário autoriza a Oneer a enviar
          notificações administrativas no Aplicativo (“push”), por e-mail, SMS,
          em uma publicação em seu site ou por qualquer outro meio de
          comunicação disponível (“Meios de Comunicação”) com conteúdo de
          natureza informativa e promocional relacionados aos Serviços.
        </p>
        <br />
        <p>
          <strong id="Elegibilidade">
            2. Elegibilidade, Utilização da Plataforma Oneer, Verificação de
            Membro
          </strong>
        </p>
        <p>
          2.1. É necessário que você tenha pelo menos 18 anos e que seja capaz
          de celebrar contratos vinculantes para acessar e utilizar a Plataforma
          Oneer ou registrar uma Conta Oneer. Ao acessar ou utilizar a
          Plataforma Oneer, você representa e garante que você possui 18 anos ou
          mais e que tem capacidade e autoridade legal para celebrar um
          contrato.
        </p>
        <p>
          2.2. A Oneer pode tornar o acesso e o uso da Plataforma Oneer ou
          certas áreas ou recursos da Plataforma Oneer sujeitas a determinadas
          condições ou exigências, como a conclusão de um processo de
          verificação, o atendimento a critérios de elegibilidade ou qualidade
          específicos, o atendimento de princípios de Avaliações ou Comentários,
          ou histórico de reserva e cancelamento de um Membro.
        </p>
        <p>
          2.3. É difícil a verificação do Usuário na Internet, e não assumimos a
          responsabilidade pela confirmação de qualquer identidade de um Membro.
          Não obstante o disposto acima, para fins de transparência e prevenção
          de fraude, e conforme permitido pelas leis aplicáveis, nós podemos,
          mas não temos a obrigação de: (i) solicitar aos Membros que forneçam
          alguma identificação oficial ou outras informações ou fazer inspeções
          projetadas para ajudar a verificar as identidades e os históricos de
          Membros; (ii) fazer uma amostragem de Membros comparada a bancos de
          dados de terceiros ou outras fontes e solicitar relatórios de
          prestadores de serviço; e, (iii) quando tivermos informações
          suficientes para identificar um Membro, obter relatórios de registros
          públicos de condenações criminais ou registros equivalentes na
          jurisdição local (se disponível).
        </p>
        <p>
          2.4. O acesso ou uso de determinadas áreas ou de certos recursos da
          Plataforma Oneer pode estar sujeito a políticas, padrões ou diretrizes
          separadas, ou podem exigir que você aceite termos e condições
          adicionais. Em caso de conflito entre estes Termos e os termos e
          condições aplicáveis a uma área ou a um recurso específico da
          Plataforma Oneer, os últimos termos e condições prevalecerão com
          relação ao acesso e uso da referida área ou recurso, exceto se
          especificado em contrário.
        </p>
        <p>
          2.5. Caso você acesse ou faça download do Aplicativo da Apple App
          Store, você concorda com o Contrato de Licença de Usuário Final de
          Aplicativo Licenciado da Apple. Algumas partes da Plataforma Oneer
          implantam os serviços de mapa do Google Maps/Earth, incluindo API (s)
          do Google Maps. Seu uso do Google Maps/Earth está sujeito aos Termos
          de Serviço Adicionais do Google Maps/Google Earth.
        </p>
        <br />
        <p>
          <strong>3. Códigos Promocionais</strong>
        </p>
        <p>
          3.1. A Oneer poderá, a seu exclusivo critério, criar códigos
          promocionais que poderão ser resgatados para crédito na Conta ou
          outras características ou benefícios relacionados aos Serviços e/ou a
          serviços de parceiros da Oneer, sujeitos a quaisquer condições
          adicionais que a Oneer estabelecer para cada um dos códigos
          promocionais (“Códigos Promocionais”).
        </p>
        <p>
          3.2. O Usuário concorda que Códigos Promocionais: (i) devem ser usados
          de forma legal para a finalidade e o público a que se destinam; (ii)
          não devem ser duplicados, de qualquer forma vendidos, transferidos ou
          disponibilizados ao público em geral (seja por meio de postagem ao
          público ou qualquer outro método), a menos que expressamente permitido
          pela Oneer; (iii) poderão ser desabilitados pela Oneer a qualquer
          momento por motivos legalmente legítimos, sem que disto resulte
          qualquer responsabilidade para a Oneer; (iv) somente poderão ser
          usados de acordo com as condições específicas que a Oneer estabelecer
          para esses Código Promocional; (v) não são válidos como dinheiro; e
          (vi) poderão expirar antes de serem usados. A Oneer se reserva o
          direito de reter ou deduzir créditos ou outras funcionalidades ou
          vantagens obtidas por meio do uso dos Códigos Promocionais pelo
          próprio Usuário ou por terceiros, caso a Oneer apure ou acredite que o
          uso ou resgate do Código Promocional foi feito com erro, fraude,
          ilegalidade ou violação às condições do respectivo Código Promocional,
          de regulamentos específicos e dos Termos, a critério da Oneer.
        </p>
        <br />
        <p>
          <strong>4. Licença</strong>
        </p>
        <p>
          4.1. Sujeito ao cumprimento destes Termos, a Oneer outorga ao Usuário
          uma licença, gratuita, limitada, não exclusiva, pessoal,
          intransferível e revogável única e exclusivamente para acessar e
          utilizar o Serviço para fins pessoais, não comerciais por meio de
          qualquer dispositivo compatível.
        </p>
        <br />
        <p>
          <strong>5. Alteração destes Termos</strong>
        </p>
        <p>
          5.1. A Oneer se reserva ao direito de modificar estes Termos a
          qualquer momento de acordo com esta provisão. Caso alteremos estes
          Termos, publicaremos os Termos revisados na Plataforma Oneer e
          atualizaremos a data de “Última Atualização” no início destes termos.
          Também enviaremos a você, por e-mail, uma notificação das alterações
          pelo menos 30 (trinta) dias antes da data de vigência da alteração.
          Caso você discorde dos Termos alterados, você pode rescindir este
          Contrato com efeito imediato. Nós informaremos sobre seu direito de
          rescindir o Contrato no e-mail de notificação. Caso você não rescinda
          seu Contrato antes da data de vigência dos Termos alterados, seu
          acesso contínuo ou uso da Plataforma Oneer constituirá aceitação dos
          Termos alterados.
        </p>
        <br />
        <p>
          <strong>6. Registro de Conta</strong>
        </p>
        <p>
          6.1. Para utilizar grande parte dos Serviços, o Membro deve
          registrar-se e manter apenas uma conta pessoal de Membro (“Conta”). O
          Membro deve ter capacidade civil e deverá cumprir todos os requisitos
          legais correspondentes ao local de sua atuação para exercício da
          atividade de locação e a que pretende desempenhar no local reservado.
          O Membro que desejar utilizar o Serviço deverá obrigatoriamente
          preencher os campos de cadastro exigidos, inclusive relativos aos
          dados necessários para recebimento de valores em decorrência da
          reserva do espaço disponibilizado, intermediado pela Oneer, e
          responderá pela veracidade das informações declaradas, obrigando-se a
          manter informações válidas, atualizadas e corretas. Em caso de não
          confirmação de seus dados, o acesso do Membro ao Serviço poderá ser
          bloqueado, a exclusivo critério da Oneer.
        </p>
        <p>
          6.2. Após receber a documentação de cadastro, a Oneer efetuará uma
          análise e poderá aceitar ou recusar a solicitação de cadastro do
          Membro, bem como de seu anúncio. A Oneer também poderá realizar a
          checagem de antecedentes criminais e quaisquer outras verificações que
          considerar oportunas ou que sejam exigidas pela legislação aplicável.
        </p>
        <p>
          6.3. O perfil do Membro é exclusivo e intransferível. O Membro
          compromete-se, mediante aceitação dos Termos, a não compartilhar sua
          Conta com terceiros, sendo vedada a transferência de sua Conta, sob
          pena de cancelamento imediato da Conta do Membro, além de
          encaminhamento do caso às autoridades públicas para análise de
          eventuais penalidades criminais e civis aplicáveis.
        </p>
        <p>
          6.4. A Oneer se reserva o direito de solicitar documentos adicionais
          para confirmação de cadastros, bem como outros métodos de
          identificação e autenticação do Membro (como, por exemplo,
          reconhecimento facial), por ocasião do cadastro e enquanto o Membro
          utilizar os Serviços a qualquer tempo. Mesmo após a confirmação do
          cadastro, é possível o cancelamento da conta caso sejam verificadas
          incongruências no processo de verificação, a exclusivo critério da
          Oneer.
        </p>
        <p>
          6.5. As informações da Conta são de exclusiva responsabilidade de quem
          as inseriu. No caso de acarretarem danos ou prejuízos de qualquer
          espécie, as medidas cabíveis podem ser tomadas pela Oneer a fim de
          resguardar seus interesses e a integridade de Usuários, demais
          Parceiros do Aplicativo e da própria Oneer.
        </p>
        <p>
          6.6. O Membro se compromete, sob as penas da lei, a utilizar sua Conta
          do Aplicativo apenas de maneira e para fins estritamente legais,
          legítimos e permitidos por estes Termos de Uso.
        </p>
        <p>
          6.7. Você deve cadastrar uma conta (“Conta Oneer”) para acessar e
          utilizar determinados recursos da Plataforma Oneer, como publicar ou
          fazer a reserva de um Anúncio. Caso você faça o cadastro de uma Conta
          Oneer para uma pessoa jurídica, você representa e garante que você
          possui autoridade para vincular legalmente tal entidade e nos concede
          todas as permissões e licenças dispostas nestes Termos.
        </p>
        <p>
          6.8. Você pode cadastrar uma Conta Oneer utilizando um endereço de
          e-mail e criando uma senha, ou através de sua conta com determinados
          serviços de rede social de terceiros, como Facebook ou Google (“Conta
          SNS”[Social Networking Services (Serviços de Rede Social)]). Você tem
          a possibilidade de desativar a conexão entre sua Conta Oneer e sua
          Conta SNS a qualquer momento, ao acessar a seção “Configurações” da
          Plataforma Oneer.
        </p>
        <p>
          6.9. Você deve fornecer informações atuais, precisas e completas
          durante o processo de registro e manter as informações de sua Conta
          Oneer e da página de seu perfil da Conta Oneer sempre atualizadas.
        </p>
        <p>
          6.10. Você não pode cadastrar mais de 1 (uma) Conta Oneer, exceto se a
          Oneer lhe autorizar a fazê-lo. Você não pode atribuir ou transferir
          sua Conta Oneer para terceiros.
        </p>
        <p>
          6.11. Você é responsável por manter a confidencialidade e a segurança
          de suas credenciais de Conta Oneer e não pode divulgar as referidas
          credenciais a terceiros. Você deve notificar a Oneer imediatamente
          caso você tome conhecimento, ou tenha qualquer motivo para suspeitar,
          de que suas credenciais foram extraviadas, furtadas, indevidamente
          apropriadas ou ainda comprometidas ou em caso de qualquer uso ou
          suspeita de uso não autorizado de sua Conta Oneer. Você é responsável
          por toda e qualquer atividade conduzida através de sua Conta Oneer,
          exceto se as referidas atividades não forem autorizadas por você e
          você não tenha sido negligente (como não relatar o uso não autorizado
          ou a perda de suas credenciais).
        </p>
        <p>
          6.12. A Oneer pode habilitar recursos que lhe permitam autorizar
          outros Membros ou determinados terceiros a realizar certas ações que
          afetem sua Conta Oneer. Por exemplo, nós podemos permitir que Membros
          elegíveis ou determinados terceiros reservem Anúncios para outros
          Membros, ou podemos permitir que os Usuários adicionem outros Membros
          como co-usuários (conforme definido abaixo) para ajudar a gerenciar
          seus Anúncios. Esses recursos não exigem que você compartilhe suas
          credenciais com outra pessoa. Nenhum terceiro é autorizado pela Oneer
          a pedir suas credenciais, e você não deve solicitar as credenciais de
          outro Membro.
        </p>
        <br />
        <p>
          <strong>7. Dados do Membro e Privacidade</strong>
        </p>
        <p>
          7.1. A Oneer possui uma política expressa sobre privacidade. As
          informações de registro e outras informações sobre o membro estão
          sujeitas ao tratamento referido em tal política de privacidade. O
          membro entende e concorda que o seu uso e a prestação do serviço
          envolvem a coleta e utilização de informações e dados sobre o membro
          (conforme definido na política de privacidade aplicável), incluindo a
          transferência destas informações e dados para outros territórios, para
          fins de armazenamento, processamento e utilização pela Oneer, sua
          controladora e demais empresas do mesmo grupo econômico, para envio às
          autoridades competentes para fins de realização de cadastramento dos
          Membros e Parceiros, para as finalidades da prestação do serviço e
          demais previstas na política de privacidade. Favor consultar a
          política de privacidade da Oneer no seguinte link: https://
          oneerapp.com/legal/privacidade. A política de privacidade da Oneer
          constitui parte integrante dos termos.
        </p>
        <br />
        <p>
          <strong>8. Conteúdo</strong>
        </p>
        <p>
          8.1. A Oneer pode, a seu próprio critério, permitir que os Membros:
          (i) criem, façam upload, publiquem, enviem, recebam e armazenem
          conteúdo, como textos, fotos, áudio, vídeo, ou outros materiais e
          informações na ou através da Plataforma Oneer (“Conteúdo de Membro”);
          e, (ii) acessar e visualiza o Conteúdo de Membro e qualquer conteúdo
          que a Oneer disponibilize na, ou através da, Plataforma Oneer,
          incluindo o conteúdo de propriedade da Oneer e qualquer conteúdo
          licenciado ou autorizado para uso por ou através da Oneer por um
          terceiro (“Conteúdo da Oneer” e, em conjunto com o Conteúdo de Membro,
          “Conteúdo Coletivo”).
        </p>
        <p>
          8.2. A Plataforma Oneer, o Conteúdo da Oneer e o Conteúdo de Membro
          podem, em sua totalidade ou em parte, ser protegidos por direitos
          autorais, marcas comerciais e/ou outras leis brasileiras. Você
          reconhece e concorda que a Plataforma Oneer e o Conteúdo da Oneer,
          incluindo todos os direitos de propriedade intelectual associados, são
          propriedade exclusiva da Oneer e/ou de seus licenciadores ou terceiros
          autorizados. Você não deve remover, alterar ou ocultar nenhum direito
          autoral, marca comercial, marca de serviço ou outros direitos de
          propriedade incorporados ou que acompanhem a Plataforma Oneer, o
          Conteúdo da Oneer ou o Conteúdo de Membro. Todas as marcas comerciais,
          marcas de serviço, logotipos, nomes comerciais e quaisquer outros
          identificadores de fonte da Oneer usados em, ou em relação à
          Plataforma Oneer e ao Conteúdo da Oneer são marcas comerciais ou
          marcas registradas da Oneer no Brasil. Marcas registradas, marcas de
          serviço, logotipos, nomes comerciais e quaisquer outras designações de
          propriedade de Terceiros utilizadas ou em relação à Plataforma Oneer,
          ao Conteúdo da Oneer e/ou ao Conteúdo Coletivo e ao Conteúdo da Oneer,
          são utilizadas somente para objetivos de identificação e podem ser de
          propriedade de seus respectivos proprietários.
        </p>
        <p>
          8.3. Você não deve utilizar, copiar, adaptar, modificar, preparar
          trabalhos derivados, distribuir, licenciar, vender, transferir, exibir
          publicamente, realizar publicamente, transmitir ou explorar a
          Plataforma Oneer ou o Conteúdo Coletivo, exceto dentro dos limites em
          que você for o proprietário de determinado Conteúdo de Membro ou
          conforme expressamente permitido nestes Termos. Nenhuma licença ou
          direitos são concedidos por implicação ou de outra forma sob quaisquer
          direitos de propriedade intelectual pertencentes ou controlados pela
          Oneer ou por seus licenciadores, salvo para as licenças e direitos
          expressamente concedidos nestes Termos.
        </p>
        <p>
          8.4. Sujeito à sua conformidade com estes Termos, a Oneer lhe concede
          uma licença intransferível, revogável, não sub licenciável, não
          exclusiva e limitada para: (i) fazer download e utilizar o Aplicativo
          em seu (s) dispositivo (s) pessoal (is); e, (ii) acessar e visualizar
          qualquer Conteúdo Coletivo disponibilizado na, ou através da,
          Plataforma Oneer e acessível a você, apenas para seu uso pessoal e não
          comercial.
        </p>
        <p>
          8.5. Ao criar, fazer upload, publicar, enviar, receber, armazenar ou
          disponibilizar qualquer Conteúdo de Membro na, ou através da,
          Plataforma Oneer, você concede à Oneer uma licença mundial
          transferível, sub licenciável, perpétua (ou pelo período da proteção),
          irrevogável, livre de royalties, não exclusiva sobre o referido
          Conteúdo de Membro para acessar, utilizar, armazenar, copiar,
          modificar, elaborar trabalhos derivativos de, distribuir, publicar,
          transmitir, encaminhar ou explorar de qualquer forma o referido
          Conteúdo de Membro para fornecer e/ou promover a Plataforma Oneer, em
          qualquer meio ou plataforma. Exceto se você der consentimento
          específico, a Oneer não reivindica quaisquer direitos de propriedade
          sobre qualquer Conteúdo de Membro e nada presente nestes Termos será
          considerado para restringir quaisquer direitos que você possa ter de
          usar ou explorar seu Conteúdo de Membro.
        </p>
        <p>
          8.6. A Oneer pode oferecer aos Usuários a opção de que fotógrafos
          profissionais façam fotografias de seus Serviços de Locação,
          disponibilizadas pelo fotógrafo aos Usuários para incluir em seus
          Anúncios, com ou sem uma marca d'água ou etiqueta contendo as palavras
          “Foto Verificada pela Oneer.app” ou algo semelhante (“Imagens
          Verificadas”). Você é responsável por garantir que seu Serviço de
          Locação seja representado precisamente nas Imagens Verificadas e que
          você cessará o uso das Imagens Verificadas na, ou através da,
          Plataforma Oneer caso elas não mais representem precisamente seu
          Anúncio, caso você pare de oferecer o Serviço de Locação
          caracterizado, ou caso sua Conta Oneer seja encerrada ou suspensa por
          qualquer motivo. Você reconhece e concorda que a Oneer terá o direito
          de utilizar quaisquer Imagens Verificadas para publicidade, marketing
          e/ou quaisquer outros fins de negócios em qualquer meio de comunicação
          ou plataforma, seja em relação ao seu Anúncio ou de outra forma, sem
          notificação ou remuneração adicional para você. Quando a Oneer não for
          a proprietária exclusiva das Imagens Verificadas, ao utilizar as
          referidas Imagens Verificadas na, ou através da, Plataforma Oneer,
          você concede à Oneer uma licença exclusiva, mundial, livre de
          royalties, irrevogável, perpétua (ou pelo período de proteção), sub
          licenciável e transferível, para utilizar as referidas Imagens
          Verificadas para publicidade, marketing e/ou quaisquer outros fins de
          negócios em qualquer meio de comunicação ou plataforma, seja em
          relação ao seu Anúncio ou de outra forma, sem notificação ou
          remuneração adicionais para você. Por sua vez, a Oneer lhe concede uma
          licença limitada, não exclusiva, não sub licenciável, revogável,
          intransferível para utilizar as Imagens Verificadas fora da Plataforma
          Oneer exclusivamente para seu uso pessoal e não comercial.
        </p>
        <p>
          8.7. Você é exclusivamente responsável por todo Conteúdo de Membro que
          você disponibilizar na, ou através da, Plataforma Oneer. Assim, você
          declara e garante que: (i) ou você é o proprietário único e exclusivo
          de todo o Conteúdo de Membro que você disponibilizar na ou através da
          Plataforma Oneer ou você tem todos os direitos, as licenças,
          autorizações e liberações necessárias para conceder à Oneer os
          direitos sobre o referido Conteúdo de Membro, conforme contemplado
          nestes Termos; e, (ii) nem o Conteúdo de Membro, nem a publicação, o
          upload, a apresentação ou transmissão do Conteúdo de Membro, ou o uso
          do Conteúdo de Membro pela Oneer (ou qualquer parte dele) infringirá,
          desapropriará ou violará uma patente de terceiro, direitos autorais,
          marca registrada, segredo comercial, direitos morais ou outros
          direitos proprietários ou de propriedade intelectual, ou direitos de
          publicidade ou privacidade, ou resultar na violação de qualquer lei ou
          regulamento aplicável.
        </p>
        <p>
          8.8. Você não publicará, fará upload, enviará ou transmitirá qualquer
          Conteúdo de Membro que: (i) seja fraudulento, falso, enganoso
          (diretamente ou por omissão ou não atualização de informações) ou
          depreciativo; (ii) seja difamatório, calunioso, obsceno, pornográfico,
          vulgar ou ofensivo; (iii) promova discriminação, intolerância,
          racismo, ódio, assédio ou que prejudique qualquer indivíduo ou grupo;
          (iv) seja violento ou ameaçador ou promova violência ou ações que
          sejam ameaçadoras a qualquer pessoa ou animal; (v) promova substâncias
          ou atividades prejudiciais ou ilegais; ou, (vi) viole a Política de
          Conteúdo da Oneer ou qualquer outra política da Oneer. A Oneer pode,
          sem qualquer aviso, remover ou desabilitar o acesso a qualquer
          Conteúdo de Membro que a Oneer considere uma violação a estes Termos
          ou às Políticas ou aos Padrões atuais da Oneer, ou que possa ser
          repreensível ou prejudicial à Oneer, seus Membros, terceiros ou à sua
          propriedade.
        </p>
        <p>
          8.9. A Oneer respeita a lei de direitos autorais e espera que seus
          Membros façam o mesmo. Caso você acredite que qualquer conteúdo na
          Plataforma Oneer infrinja direitos autorais de sua propriedade,
          notifique-nos de acordo com nossa Política de Direitos Autorais.
        </p>
        <br />
        <p>
          <strong>
            9. Responsável pelo Processamento do Pagamento e pelos Dados de
            Pagamento
          </strong>
        </p>
        <p>
          9.1. Você é responsável por pagar quaisquer Taxas de Serviço que você
          deva à Oneer. As Taxas de Serviço aplicáveis (incluindo eventuais
          Impostos aplicáveis) são recolhidas pela Wirecard Brazil S.A., pessoa
          jurídica de direito privado regularmente inscrita no CNPJ/MF sob n.
          08.718.431/0001-08, domiciliada à Av. Brigadeiro Faria Lima, 3064 -
          Itaim Bibi - SP, 12º andar, telefones: (11) 3181-8180 ou 0800 326 0809
          (somente chamadas de telefone fixo).
        </p>
        <p>
          9.2. A Wirecard coleta diversas informações pessoais suas e, por essa
          razão, a segurança de seus dados é prioridade. A Wirecard nunca
          compartilhará seus dados pessoais, a menos que:
        </p>
        <p>
           Você peça ou nos autorize a compartilhar tais dados com terceiros;
        </p>
        <p>
           Sejamos obrigados a disponibilizar os dados em razão de lei ou ordem
          judicial;
        </p>
        <p>
           Sejam necessárias para prestarmos o serviço da forma que você
          contratou; e
        </p>
        <p>
           Sejam necessárias para concluir a requisição de pagamento ou
          recebimento efetuada por você;
        </p>
        <p>
          9.3. Nós, Wirecard e a Oneer, cumprem rigorosamente a legislação
          aplicável a dados pessoais, em especial o Marco Civil da Internet (Lei
          nº 12.965/14), e usamos medidas físicas e digitais de segurança, como
          controle de acessos, criptografia forte, implementação SSL, firewalls,
          registro de log de alteração, duplo fator de autenticação, entre
          outros mecanismos de proteção.
        </p>
        <p>
          9.4. Para sua maior tranquilidade, a Wirecard possui o Departamento de
          Segurança da Informação que é focado somente em prevenir e combater
          ameaças. É por essas e outras razões que a Wirecard foi certificado
          pelo Payment Card Industry Security Standards Council como uma
          instituição segura, comparável às líderes mundiais na área de
          pagamentos. Infelizmente, não podemos garantir a segurança do aparelho
          pelo qual você acessa a Wirecard, por isso tome sempre as precauções
          de segurança básicas:
        </p>
        <p> Não confie em e-mails estranhos;</p>
        <p> Não acesse sites suspeitos;</p>
        <p>
           Tenha mecanismos de proteção ativos e atualizados, tais como;
          antivírus e anti-malware;
        </p>
        <p> Não instale programas de fontes estranhas ou ilegais;</p>
        <p>
          9.5. A Wirecard preza em manter os dados pessoais que coleta em sigilo
          e fará todo o possível para garantir que a confiança que você pôs em
          nós seja justificada.
        </p>
        <p>
          9.6. Se a nossa parceria algum dia chegar ao fim, saiba que é seu
          direito solicitar a destruição dos seus dados pessoais que a Wirecard
          possui. Infelizmente, em decorrência da Lei nº 9.613/98, somos
          obrigados a armazenar os seus dados pessoais, inclusive aqueles
          relacionados às transações efetuadas, por pelo menos 5 (cinco) anos,
          extensíveis mediante solicitação do Ministério Público ou autoridades
          administrativas, e não poderemos excluir informações antes deste prazo
          mesmo que você pedir.
        </p>
        <p>
          9.7. Contudo, fique tranquilo: a Wirecard armazenará sua solicitação
          de esquecimento e, uma vez terminado o prazo legal, providenciará a
          destruição das informações capazes de te identificar. Seus dados
          somente não serão apagados ao fim do prazo legal se você ainda possuir
          alguma obrigação pendente com a Wirecard, como taxas não pagas, saldo
          negativo. Nesta hipótese, manteremos suas informações em nosso banco
          de dados como forma de facilitar a solução da pendência.
        </p>
        <p>
          9.8. Você reconhece e concorda com os termos desta política de
          privacidade. Reconhece também que são aplicáveis as leis do brasil
          para tratar suas informações. Bem como reconhece que esta política de
          privacidade é complementar ao contrato da empresa Wirecard.
        </p>
        <br />
        <p>
          <strong>10. Taxas de Serviço</strong>
        </p>
        <p>
          10.1. A Oneer pode cobrar taxas do Locatário (“Taxas do Locação”) e/ou
          Locador (“Taxas do Locador”) (coletivamente, “Taxas de Serviço”) em
          consideração ao uso da Plataforma Oneer. Mais informações sobre quando
          são aplicadas taxas de Serviço e como são calculadas, podem ser
          encontradas em nossa página de Taxas de Serviço.
        </p>
        <p>
          10.2. Quaisquer Taxas de Serviço aplicáveis (incluindo quaisquer
          Impostos aplicáveis) serão apresentadas a um Locatário ou Locador
          antes da publicação ou reserva de um Anúncio. A Oneer reserva-se ao
          direito de alterar as Taxas de Serviço a qualquer momento, e nós
          notificaremos os Membros com antecedência adequada quanto a quaisquer
          alterações de taxa antes de serem aplicadas.
        </p>
        <p>
          10.3. A Wirecard Brazil S.A. deduzirá eventuais Taxas do Locação da
          Taxa do Anúncio antes de repassar o pagamento ao Locador. Eventuais
          Taxas do Locatário são incluídas nas Taxas Totais recolhidas pela
          Wirecard Brazil S.A. Exceto se previsto em contrário na Plataforma
          Oneer, as Taxas de Serviço não são reembolsáveis.
        </p>
        <p>
          10.4.&nbsp;O pagamento da reserva é feito diretamente pelo (s) Usuário
          (s) ao Locador, por uma das modalidades de pagamento escolhidas pelo
          Usuário, sendo cobrado do Locador o valor da Remuneração pela
          Intermediação, conforme previsto neste Termo. O Membro pode optar por
          receber pagamentos em uma ou mais das seguintes modalidades: (i)
          dinheiro; (ii) em sua própria máquina de cartão de débito ou crédito;
          (iii) em cartão por meio do próprio Aplicativo e/ou (iv) outros meios
          de pagamento disponíveis no Aplicativo. O pagamento por meio do
          próprio Aplicativo é o único que o Membro é obrigado a aceitar pelos
          Termos de Uso.
        </p>
        <p>
          10.5. O valor a ser pago pelo Usuário ao Locador é calculado com base
          na legislação aplicável na região de realização da reserva. Este valor
          será previamente exposto nos Anúncios.
        </p>
        <p>
          10.6. A Oneer poderá disponibilizar aos Usuários, por meio do
          Aplicativo, um desconto variável no valor das reservas (“Modo
          Desconto”). O Locador poderá aceitar ou não as reservas do Modo
          Desconto a ele oferecidas.
        </p>
        <p>
          10.7. No momento em que o (a) Usuário (a) tenha inserido os dias e/ou
          horários e/ou períodos que pretende reservar sobre determinado local
          por meio do Aplicativo, receberá o valor do preço pela reserva
          correspondente (“Preço”). O Preço será composto por: (i) valor
          base/mínimo; acrescido dos (ii) impostos incidentes (iii) parcela
          variável baseada nas datas e quantidade de tempo estimado da reserva.
          Além disso, em certas situações poderá ser oferecido um desconto. O
          Desconto será parte integrante do Preço e sua incidência será clara.
        </p>
        <p>
          10.8. A Oneer poderá cobrar por todo ou parte do Serviço, ao seu
          critério. Atualmente, o Licenciamento é feito a título gratuito, sendo
          que a Intermediação é prestada de maneira onerosa (“Remuneração pela
          Intermediação”). Como remuneração pela Intermediação, a Oneer cobrará
          do Membro um valor por cada reserva realizada, de acordo com a
          categoria do serviço.
        </p>
        <p>
          10.9. A Oneer disponibilizará ao Membro um recibo com o respectivo
          valor da Remuneração pela Intermediação. O Membro concorda que o valor
          a ser pago por ele a título de Remuneração pela Intermediação será o
          disponibilizado no recibo em caso de qualquer conflito ou
          inconsistência.
        </p>
        <p>
          10.10. Para reservas pagas pelo Membro por meio do Aplicativo, o
          pagamento da Remuneração pela Intermediação será realizado
          automaticamente por meio do Aplicativo e dos meios de pagamento
          relacionados. Para as reservas pagas por outro (s) meio (s) o, a
          cobrança dos valores devidos à Oneer será realizada na próxima reserva
          cujo pagamento for realizado por intermédio do Aplicativo. Na hipótese
          de valores pendentes de forma reiterada, a Oneer poderá ao seu
          critério (i) emitir contra o Membro a respectiva fatura / nota fiscal
          para o pagamento dos valores em aberto, à qual o Membro deverá
          proceder com o pagamento na data de vencimento indicada e/ou (ii)
          bloquear o cadastro do Membro até que os valores sejam pagos.
        </p>
        <br />
        <p>
          <strong>11. Termos específicos para Usuários</strong>
        </p>
        <p>
          <strong>11.1. Termos aplicáveis a todos os Anúncios</strong>
        </p>
        <p>
          11.1.1 Ao criar um Anúncio através da Plataforma Oneer você deve: (i)
          fornecer informações completas e precisas sobre seus Serviços de
          Locação (tais quais descrição do anúncio, localização, equipamentos,
          dimensões, capacidade e disponibilidade de calendário); (ii) divulgar
          quaisquer deficiências, restrições (como as regras condominiais) e
          exigências que se apliquem (como capacidade do local, equipamentos,
          horários de atendimento/funcionamento, toda e qualquer informação
          relevante para o uso do espaço físico ofertado); e, (iii) fornecer
          quaisquer outras informações pertinentes solicitadas pela Oneer. Você
          é responsável por manter as informações do seu Anúncio atualizadas a
          todo o momento (incluindo a disponibilidade de calendário).
        </p>
        <p>
          11.1.2 Você é o único responsável por estabelecer um preço (incluindo
          quaisquer Impostos, se aplicável, ou cobranças como taxas de limpeza)
          para seu Anúncio (“Taxa do Anúncio”). Uma vez que um Locatário
          solicitar a Reserva de seu Anúncio, você não poderá solicitar ao
          Locatário que pague um preço maior do que aquele do pedido de Reserva.
        </p>
        <p>
          11.1.3. Quaisquer termos e condições incluídos em seu Anúncio,
          especialmente com relação a cancelamentos, não devem estar em conflito
          com estes Termos ou com a Política de cancelamento pertinente que você
          selecionou para seu Anúncio.
        </p>
        <p>
          11.1.4. As fotos, animações ou os vídeos (coletivamente, “Imagens”)
          utilizados em seus Anúncios devem refletir precisamente a qualidade e
          a condição de seus Serviços de Locação. A Oneer reserva-se ao direito
          de exigir que os Anúncios possuam um número mínimo de Imagens de um
          determinado formato, tamanho e resolução.
        </p>
        <p>
          11.1.5. A colocação e classNameificação de Anúncios em resultados de
          pesquisa na Plataforma Oneer podem variar dependendo de uma variedade
          de fatores, tais quais as preferências e os parâmetros de busca do
          Locatário, as exigências da Locação, a disponibilidade de calendário e
          preço, o número e a qualidade das Imagens, o atendimento ao cliente e
          o histórico de cancelamento, os Comentários e as Avaliações, o tipo de
          Serviço de Locação e/ou a facilidade de reserva.
        </p>
        <p>
          11.1.6. Quando você aceita ou quando tiver pré-aprovado uma
          solicitação de reserva por um Locatário, você celebra um contrato
          legalmente vinculante com o Locatário, e você deve fornecer seu (s)
          Serviço (s) de Locação ao Locatário conforme descrito em seu Anúncio
          quando a solicitação de reserva for feita. Você também concorda em
          pagar a (s) Taxa (s) de Locação aplicável (is) e eventuais Impostos
          aplicáveis.
        </p>
        <p>
          11.1.7. A Oneer recomenda que a Locador obtenha o seguro apropriado
          para seus Serviços de Locação. Examine cuidadosamente cada apólice de
          seguro, e, em particular, certifique-se de que entende e está
          familiarizado com todas as exclusões, e quaisquer deduções que possam
          ser aplicadas para tal apólice de seguro, incluindo, mas não limitando
          a, se a sua apólice de seguro cobrirá ou não as ações ou omissões de
          Locatários (e as pessoas para quem o Locatário reservou seu imóvel
          comercial, se aplicável).
        </p>
        <br />
        <p>
          <strong>11.2 Anúncios de Imóveis Comerciais</strong>
        </p>
        <p>
          11.2.1 A menos que seja expressamente permitido pelo Oneer você não
          pode anunciar mais de uma Imóvel por Anúncio.
        </p>
        <p>
          11.2.2 Se você optar por solicitar um depósito de segurança para seu
          Imóvel comercial, você deve especificar isso em seu Anúncio (“Depósito
          de Segurança”). Os Usuários não podem solicitar um Depósito de
          Segurança fora da Plataforma Oneer ou após uma reserva ter sido
          confirmada. A Oneer desprenderá de esforços comercialmente razoáveis
          para resolver solicitações e reivindicações dos Usuários relacionados
          aos Depósitos de Segurança, mas a Oneer não é responsável pela
          administração ou aceitação de quaisquer reivindicações de Usuários
          relacionados aos Depósitos de Segurança.
        </p>
        <p>
          11.2.3. Você declara e garante que qualquer Anúncio que você publicar
          e que a reserva de, ou a estadia de um Locatário em um imóvel
          comercial: (i) não quebrará qualquer contrato que você tenha celebrado
          com terceiros, como associação de proprietários, condomínio, ou outros
          contratos; e, (ii) cumprirão todas as leis aplicáveis (como leis de
          zoneamento), exigências de impostos e outras regras e regulamentos
          (incluindo ter todas as permissões, licenças e os registros exigidos).
          Enquanto Locador, você é responsável por seus próprios atos e omissões
          e também é responsável pelos atos e omissões de qualquer indivíduo que
          de algum modo esteja presente no imóvel comercial locado.
        </p>
        <br />
        <p>
          <strong>
            11.3 Anúncios de Experiências, Eventos e outros Serviços de Locação
          </strong>
        </p>
        <p>
          11.3.1. Usuários que anunciam Eventos e Serviços de Locação Comercial
          concordam que estão sujeitos aos Termos Adicionais para Usuários de
          Experiências.
        </p>
        <br />
        <p>
          <strong>12. Termos específicos para Locatários</strong>
        </p>
        <p>
          <strong>12.1. Termos aplicáveis a todas as reservas</strong>
        </p>
        <p>
          12.1.1. Sujeito a atender quaisquer solicitações (como preencher
          quaisquer processos de verificação) estabelecidas pelo Oneer e/ou
          Locador, você pode reservar um Anúncio disponível na Plataforma Oneer
          ao seguir o respectivo processo de reserva. Todas as taxas aplicáveis,
          incluindo a Taxa de Anúncio, o Depósito de Segurança (se aplicável),
          Taxa de Locatário e quaisquer Impostos aplicáveis (coletivamente,
          “Taxas Totais”) serão apresentados a você antes de reservar um
          Anúncio. Você concorda em pagar as Taxas Totais para qualquer reserva
          solicitada com relação à sua Conta Oneer.
        </p>
        <p>
          12.1.2. Ao receber uma confirmação de reserva da Oneer, forma-se um
          contrato legalmente vinculante entre você e seu Locacatário, sujeito a
          quaisquer termos e condições adicionais de Locação que sejam
          aplicáveis, incluindo especialmente a política de cancelamento
          aplicável e quaisquer regras e restrições especificadas no Anúncio. A
          Oneer cobrará as Taxas Totais no momento da solicitação de reserva ou
          após a confirmação do Locação conforme os Termos de Pagamento. Para
          certas reservas, os Locatários poderão ser obrigados a pagar ou ter a
          opção de pagar em várias parcelas.
        </p>
        <p>
          12.1.3. Se você reservar um Serviço de Locação em nome de Locatários
          adicionais, será solicitado que você assegure que todo Locatário
          adicional atenda a quaisquer exigências estabelecidas pelo Locador, e
          concorde e esteja ciente com estes Termos e quaisquer termos e
          condições, regras e restrições estabelecidas pelo Locação. Vedada
          realizar reserva para um Locatário menor de idade.
        </p>
        <p>
          12.1.4. A Oneer pode permitir que um Locatário que esteja reservando
          um Anúncio em nome de um ou mais Locatários adicionais (o
          "Organizador") divida o pagamento das Taxas Totais para uma reserva
          elegível de forma proporcional entre o Organizador e pelo menos mais
          um Locatário adicional (denominado individualmente de "Copagador") (o
          "Serviço de Pagamento em Grupo"). Para poder participar do Serviço de
          Pagamento em Grupo, cada Copagador precisa possuir ou criar uma conta
          Oneer antes de realizar um pagamento. Todos os pagamentos efetuados
          através do Serviço de Pagamento em Grupo são gerenciados pela Oneer e
          estão sujeitos aos Termos de Serviço do Pagamento em Grupo.
        </p>
        <p>
          12.1.5. O Membro deve respeitar todas as regras destes Termos de Uso e
          de toda legislação aplicável. O descumprimento dos Termos de Uso ou da
          legislação aplicável pelo Membro poderá resultar, a livre e exclusivo
          critério da Oneer, impedimento do seu acesso ao Aplicativo. Mediante
          aceitação dos Termos de Uso, o Membro compromete-se a:
        </p>
        <p>a) Pagar a Remuneração pela Intermediação à Oneer;</p>
        <p>
          b) Agir perante a Oneer, Parceiros e aos Membros com boa-fé,
          diligência, profissionalismo e respeito;
        </p>
        <p>
          c) Obedecer a todas as exigências legais e regulatórias referentes aos
          serviços que se propor a prestar, incluindo as leis, regulamentos e
          demais normas aplicáveis em âmbito federal, estadual e municipal,
          tanto quanto ao Membro como em relação ao espaço utilizado para o fim
          comercial disponibilizado;
        </p>
        <p>d) Não discriminar ou selecionar, por nenhum motivo, os Membros;</p>
        <p>
          e) Responsabilizar-se integralmente pela prestação do serviço que se
          propôs; e
        </p>
        <p>f) Responsabilizar-se pelo uso do espaço reservado.</p>
        <br />
        <p>
          <strong>13. Manutenção do Padrão de Qualidade Oneer</strong>
        </p>
        <p>
          13.1. O Membro aceita que será avaliado pelos Usuários e pela Oneer
          com base em critérios como a qualidade do serviço, a limpeza do local,
          as conveniências disponibilizadas, as taxas de aceite e de
          cancelamento de reservas, entre outros aspectos. O Membro que for
          reiteradamente mal avaliado poderá ter a sua licença de uso do
          Aplicativo cancelada. Sem prejuízo de outras disposições constantes
          neste instrumento, o Membro também poderá ser ter sua Conta cancelada
          (resultando em impedimento de acesso ao Aplicativo) em casos como
          pendências cadastrais, relatos de condutas inapropriadas, a exclusivo
          critério da Oneer.
        </p>
        <p>
          13.2. O Membro reconhece e aceita que a Oneer não está obrigada a
          filtrar, censurar ou controlar, nem se responsabiliza pelas avaliações
          realizadas pelos Usuários, que são os únicos e exclusivos responsáveis
          por seu conteúdo. Tais avaliações não expressam a opinião ou o
          julgamento da Oneer sobre os Membros.
        </p>
        <p>
          13.3. O Membro aceita que a Oneer manterá registros internos acerca da
          reserva comercial, tais como a taxa de aceitação e cancelamento de
          reservas, podendo utilizar esses dados para realizar sua própria
          avaliação sobre o Membro.
        </p>
        <p>
          13.4. Periodicamente, o Membro será informado sobre alterações em sua
          avaliação, que consistirá em uma nota de 1 (um) a 5 (cinco), que leva
          em consideração a avaliação e comentários dos Passageiros e da Oneer,
          conforme os critérios estabelecidos neste Termo, bem como outros
          critérios pertinentes.
        </p>
        <p>
          13.5. O Membro reconhece e aceita que a Oneer poderá: (i) suspender
          por tempo indeterminado o Licenciamento (e, consequentemente, a Conta
          do Membro), dentre outros motivos elencados neste Termo; ou (ii)
          exigir a apresentação de novos documentos, caso o Membro apresente
          avaliações semanais reiteradamente ruins, a exclusivo critério da
          Oneer.
        </p>
        <br />
        <p>
          <strong>13.2 Reserva Comercial</strong>
        </p>
        <p>
          13.2.1. Você compreende que uma reserva confirmada de um imóvel
          comercial (“Reserva Comercial”) é uma licença limitada concedida a
          você pelo Locador para entrar, ocupar e utilizar o imóvel delimitado
          no Anúncio por período determinado, tempo durante o qual o Locador
          (somente quando e na medida permitida pela lei aplicável) detiver o
          direto de entrar novamente no imóvel comercial, de acordo com seu
          contrato com o Locação.
        </p>
        <p>
          13.2.2. Caso você permaneça além do período acordado para o checkout
          sem o consentimento do Locador, você não terá mais licença para
          permanecer no imóvel, e o Locador terá o direito de requisitar sua
          saída da forma condizente com a legislação aplicável, salvo acordo
          formal entre as partes por meio do aplicativo ou site da Oneer. Além
          disso, você concorda em pagar, se solicitado pelo Locador, uma taxa
          adicional por 15 (quinze) minutos até a retirada do imóvel de até duas
          vezes a média da Taxa de Anúncio para cobrir o inconveniente sofrido
          pelo Locador, somando-se a esse valor todas as Taxas do Locatário,
          Impostos e quaisquer gastos legais incorridos pelo Locador para fazer
          você deixar o local. As Taxas de Locação Prolongada por checkouts em
          atraso na data de checkout que não impactem em reservas posteriores,
          podem ser limitadas aos custos adicionais incorridos pelo Locador como
          resultado da referida Locação Prolongada. Se você incorrer em uma
          Locação Prolongada em um imóvel comercial, você autoriza a Oneer a lhe
          fazer uma cobrança para receber Taxas de Locação Prolongada. Um
          Depósito de Segurança, se exigido pelo Locador, pode ser aplicado a
          quaisquer Taxas de Locação Prolongada.
        </p>
        <p>
          <strong>13.3 Reservas Eventos e outros Serviços de Locação</strong>
        </p>
        <p>
          13.3.1. Você deve revisar atentamente a descrição de qualquer Evento
          ou outro Serviço de Locação que você pretenda reservar para garantir
          que você (e quaisquer Locatários adicionais para quem você esteja
          fazendo uma reserva) atenda quaisquer exigências que o Locação tenha
          especificado em seu Anúncio. Você é responsável por identificar,
          compreender e cumprir com todas as leis, regras e regulamentações que
          se apliquem à sua participação ao utilizar o Serviço de Locação.
        </p>
        <p>
          13.3.2. Antes e durante o uso do Serviço de Locação, você deve aderir
          a todo o momento às instruções do Locador.
        </p>
        <br />
        <p>
          <strong>
            14. Alterações, Cancelamentos e Reembolsos de Reserva, Central de
            Resoluções
          </strong>
        </p>
        <p>
          14.1 Os Usuários são responsáveis por quaisquer alterações à reserva
          que eles façam através da Plataforma Oneer ou diretamente no
          atendimento ao cliente Oneer para fazer (“Alterações de Reserva”), e
          concorda em pagar quaisquer Taxas de Anúncio, Taxas do Locação ou
          Taxas do Locatário adicionais e/ou Impostos relacionados às referidas
          Alterações de Reserva.
        </p>
        <p>
          14.2 Os Locatários podem cancelar uma reserva confirmada a qualquer
          momento, de acordo com a política de cancelamento do Anúncio, e a
          Oneer reembolsará o valor devido pelas Taxas Totais ao Locatário
          conforme estabelecido na referida política de cancelamento. A menos
          que existam causas de força maior, qualquer parte das Taxas Totais
          devida ao Locação sob a política de cancelamento aplicável será
          repassada ao Locador pela Oneer de acordo com os Termos de Pagamento.
        </p>
        <p>
          14.3. Caso um Locador cancele uma reserva confirmada, o Locatário
          receberá um reembolso total das Taxas Totais pela reserva, dentro de
          um período de tempo comercialmente razoável após o cancelamento. Em
          algumas instâncias, o Oneer pode permitir que o Locatário aplique o
          reembolso a uma nova reserva, situação na qual a Oneer creditará o
          valor na próxima reserva do Locatário, se assim desejar. Além disso, a
          Oneer pode publicar um comentário automático no Anúncio cancelado pelo
          Locador indicando que uma reserva foi cancelada. Além disso, a Oneer
          pode (i) manter o calendário do Anúncio indisponível ou bloqueado para
          as datas da reserva cancelada, e/ou (ii) impor uma taxa de
          cancelamento, exceto se o Locador tiver uma razão válida para o
          cancelamento da reserva de acordo com a Política de Causas de Força
          Maior da Oneer ou tenha preocupações legítimas quanto ao comportamento
          do Locatário.
        </p>
        <p>
          14.4. Para os Serviços de Locação, caso situações climáticas rigorosas
          gerem um cenário inseguro e desconfortável para os Locatários, os
          Usuários podem alterar ou cancelar um Serviços de Locação. Caso ocorra
          uma alteração substancial no itinerário ou caso o Serviço de Locação
          precise ser cancelado, a Oneer trabalhará com o Locador e/ou Locatário
          para fornecer uma data alternativa para o Serviço de Locação, um
          reembolso apropriado ou um reagendamento de reserva.
        </p>
        <p>
          14.5. Em determinadas circunstâncias, a Oneer pode decidir, a seu
          exclusivo critério, que é necessário cancelar uma reserva confirmada e
          tomar decisões apropriadas de pagamento e reembolso. Isso pode ocorrer
          pelos motivos estabelecidos na Política de Circunstâncias Atenuantes
          da Oneer ou (i) quando a Oneer acreditar, de boa-fé, considerando os
          interesses legítimos de ambas as partes, que isso seja necessário para
          evitar prejuízos significativos à Oneer, a outros Membros, a terceiros
          ou à propriedade, ou (ii) para quaisquer dos motivos estabelecidos
          nestes Termos.
        </p>
        <p>
          14.6. Caso um Locatário não compareça ao imóvel locado por motivos de
          força maior, a Oneer pode determinar, a seu exclusivo critério, o
          reembolso parcial ou total de todas as Taxas Totais de acordo com a
          Política de Reembolso do Locatário.
        </p>
        <p>
          14.7. Se, como um Locador, seu Locatário cancelar uma reserva
          confirmada ou a Oneer decidir que é necessário cancelar uma reserva
          confirmada, e a Oneer emitir um reembolso ao Locador de acordo com a
          Política de Reembolso do Locatário, a Política de Causas de Força
          Maior, ou qualquer outra política de cancelamento aplicável, você
          concorda que caso já tenha recebido o pagamento, a Wirecard Brazil
          S.A. terá direito de recuperar o valor desse eventual reembolso de
          você, incluindo a dedução do reembolso em questão de eventuais
          Pagamento futuros devidos a você.
        </p>
        <br />
        <p>
          <strong>
            15. Suspensão e Cancelamento de seu Acesso ao Aplicativo
          </strong>
        </p>
        <p>
          15.1. O Membro concorda que a Oneer, à sua livre discrição, poderá
          suspender ou cancelar sua utilização do Serviço, incluindo, mas não se
          limitando: (i) por descumprimentos e/ou violação destes Termos; (ii)
          pelo resultado de sua avaliação pelos Membros e pela análise de sua
          taxa de cancelamento e outros critérios; (iii) em função de ordem
          judicial ou requisição legal de autoridade pública competente; (iv)
          por requisição do próprio Membro; (v) por desativação ou modificação
          do Serviço (ou de qualquer de suas partes); (vi) por caso fortuito,
          força maior e/ou questões de segurança; (vii) por inatividade da conta
          por um longo período de tempo; (viii) pela suposta prática de qualquer
          infração, atividade fraudulenta ou ilegal por parte do Membro, a
          critério da Oneer; (ix) pelo uso inadequado ou abusivo do Aplicativo,
          incluindo a utilização por terceiros ou transferência de sua Conta, a
          realização de reserva de local distinto do cadastrado no Aplicativo,
          utilização de quaisquer aplicativos ou programas que visem a alterar a
          informação da localização geográfica do Membro para manipular o
          Aplicativo, e outras hipóteses de uso indevido ou abusivo do
          Aplicativo, a critério da Oneer; e/ou (x) por inadimplemento por parte
          do Membro de quaisquer obrigações, valores, pagamentos devidos em
          razão do Serviço, quando aplicável.
        </p>
        <p>
          15.2. O membro concorda que o término de seu acesso ao serviço, por
          qualquer razão constante destes termos, pode ocorrer sem uma
          notificação prévia e todas as informações e dados constantes poderão
          ser permanentemente apagados.
        </p>
        <p>
          15.3. A Oneer se reserva o direito de agir judicialmente e
          extrajudicialmente em casos de danos sofridos pela Oneer ou por
          terceiros, inclusive poderá entrar em contato com as autoridades e dar
          início à instrução de processos criminais, civis e administrativos nos
          casos previstos pela lei, quando julgar necessário.
        </p>
        <p>
          15.4. A suspensão ou resilição destes Termos de Uso pela Oneer não
          isenta o Membro do pagamento de quaisquer valores eventualmente
          devidos à Oneer, que seguirão sendo passíveis de cobrança e de
          compensação.
        </p>
        <p>
          15.5. O Membro não fará jus a qualquer indenização ou compensação,
          seja pela suspensão ou resilição destes Termos de Uso pela Oneer.
        </p>
        <br />
        <p>
          <strong>16. Avaliações e Comentários</strong>
        </p>
        <p>
          16.1. Dentro de um determinado prazo após concluir uma reserva, os
          Locatários e Usuários podem deixar um comentário público
          (“Comentário”) e fazer uma avaliação por estrelas (“Avaliação”) um
          sobre o outro. Avaliações ou Comentários refletem as opiniões de
          Membros individuais e não refletem a opinião do Oneer. As Avaliações e
          os Comentários não são verificados pela Oneer quanto à precisão e
          podem ser incorretos ou enganosos.
        </p>
        <p>
          16.2. As Avaliações e os Comentários feitos pelos Locatários e
          Usuários devem ser precisos e não podem conter qualquer linguagem
          difamatória ou ofensiva. As Avaliações e os Comentários estão sujeitos
          à Seção 5 e devem estar em conformidade com a Política de Conteúdo e
          com a Política de Extorsão da Oneer.
        </p>
        <p>
          16.3. É proibido aos Membros manipular o sistema de Avaliações e
          Comentários de qualquer forma, como instruir um terceiro a escrever um
          Comentário positivo ou negativo sobre o outro Membro.
        </p>
        <p>
          16.4. Avaliações e Comentários fazem parte do perfil público de um
          Membro e também podem aparecer em outros lugares da Plataforma Oneer
          (como na página do Anúncio) junto com outras informações relevantes,
          como o número de reservas, o número de cancelamentos, o tempo médio de
          resposta e outras informações.
        </p>
        <br />
        <p>
          <strong> 17. Dano ao Imóvel, Litígios entre Membros</strong>
        </p>
        <p>
          17.1 Como Locatário, você é responsável por deixar o Imóvel, incluindo
          as dependências, na condição em que estava quando você lá chegou. Você
          é responsável por seus próprios atos e omissões e também é responsável
          pelos atos e omissões de qualquer indivíduo que você convide, ou a
          quem você dê acesso ao Imóvel, exceto o Locador (e os indivíduos que o
          Locador convidar ao imóvel, se aplicável).
        </p>
        <p>
          17.2 Se um Locador reclamar e fornecer prova de que você, como
          Locatário, danificou o Imóvel comercial ou qualquer propriedade
          pessoal ou as dependências do imóvel (“Reivindicação de Danos”), o
          Locador pode solicitar pagamento de você através do Central de
          Resoluções. Se um Locador encaminhar uma Reivindicação de Danos para a
          Oneer, você terá a oportunidade de responder. Caso você concorde em
          pagar ao Locador, ou a Oneer determine, a seu exclusivo critério, que
          você é responsável por uma Reivindicação de Danos, a Oneer cobrará
          qualquer valor aí referido de você e/ou de seu Depósito de Segurança
          necessário para cobrir a Reivindicação de Dano de acordo com os Termos
          de Pagamento. A Oneer também se reserva ao direito de cobrar pagamento
          de você e buscar quaisquer remediações disponíveis à Oneer neste
          sentido em situações nas quais você for responsável por uma
          Reivindicação de Danos, incluindo, mas não se limitando a, em relação
          a quaisquer solicitações de pagamento feitas por Usuários sob a
          Garantia ao Locador da Oneer.
        </p>
        <p>
          17.3. Os Membros concordam em cooperar e ajudar a Oneer de boa-fé, e
          fornecer à Oneer essas informações e tomar as medidas que venham a ser
          razoavelmente solicitadas pela Oneer com relação a quaisquer queixas
          ou reivindicações apresentadas por Membros relativas (i) Imóvel
          comercial ou a qualquer bem pessoal, (ii) Acordos com CoUsuários ou
          (iii) uma Reserva de Pagamento em Grupo. Um Membro deve, em caso de
          solicitação razoável da Oneer e sem custos para o Membro, participar
          do processo de mediação ou de um processo de resolução semelhante com
          outro Membro, cujo processo será conduzido pela Oneer ou por um
          terceiro selecionado pela Oneer ou por seu emissor, com relação a
          perdas ou pelas quais um Membro solicite pagamento da Oneer
          (incluindo, sem limitar-se a, pagamentos sob a Garantia ao Locação da
          Oneer).
        </p>
        <p>
          17.4. Se você for um Locatário, você entende e concorda que a Oneer
          pode fazer uma reivindicação junto a sua apólice de seguro de
          proprietário ou locador ou outra apólice de seguro referente a
          qualquer dano ou perda que você possa ter causado, ou pela qual você
          possa ter sido responsável por, a qualquer propriedade pessoal ou não
          (incluindo uma Acomodação) do Locatário (incluindo, mas sem limitação,
          os valores pagos pela Oneer sob a Garantia ao Locador da Oneer). Você
          concorda em cooperar e ajudar a Oneer de boa-fé, e fornecer à Oneer
          informações conforme possam ser razoavelmente solicitadas pela Oneer,
          a fim de fazer uma reivindicação ao proprietário, locador ou outra
          apólice de seguro, incluindo, mas sem limitação, a celebração de
          documentos e tomando tais medidas posteriores que a Oneer possa
          razoavelmente solicitar para auxiliar na realização do presente.
        </p>
        <br />
        <p>
          <strong>18. Arredondamento</strong>
        </p>
        <p>
          18.1. De um modo geral, a Oneer aceita valores de pagamento devidos
          aos ou pelos Locatários ou Usuários até a menor unidade possível de
          determinada moeda (ou seja, centavos de real, centavos de dólar,
          centavos de euro ou outras moedas aceitas). Quando o provedor de
          serviços de pagamento terceirizado da Oneer não aceitar pagamentos na
          menor unidade possível de uma determinada moeda, a Oneer poderá, ao
          seu critério exclusivo, arredondar para mais ou para menos valores de
          pagamento devidos aos ou pelos Locatários ou Usuários até unidade
          básica funcional e inteira mais próxima que a moeda possua (ou seja,
          até o valor inteiro mais próximo do dólar, euro ou outra moeda
          aceita); por exemplo, a Oneer pode arredondar para cima um valor de
          R$101,50 a R$102,00 e arredondar para baixo um valor de R$101,49 a
          R$101,00.
        </p>
        <br />
        <p>
          <strong>19. Impostos</strong>
        </p>
        <p>
          19.1. Como um Locador, você é o único responsável por determinar suas
          obrigações de relatar, cobrar, remitir ou incluir em suas Taxas de
          Anúncio e Imposto aplicável ou outros impostos indiretos de venda,
          impostos de ocupação, impostos de turismo ou de outro tipo de imposto
          de visitante ou impostos de renda (“Impostos”).
        </p>
        <p>
          19.2. As regras tributárias podem nos requerer a coleta de informações
          fiscais apropriadas de Usuários, ou reter Impostos de pagamentos para
          Usuários, ou ambos. Se um Locador não nos fornecer tal documentação
          que consideremos ser suficiente para mitigar a nossa obrigação (caso
          haja) de reter impostos de pagamentos para você, reservamo-nos ao
          direito de, a nosso critério exclusivo, congelar todos os pagamentos
          que lhe forem devidos até a resolução, reter tais valores, conforme
          requerido por lei, ou ambos.
        </p>
        <p>
          19.3. Você entende que qualquer órgão governamental apropriado, bem
          como departamento e/ou autoridade (“Autoridade Fiscal”) de onde se
          localiza a sua Acomodação podem solicitar que sejam recolhidos
          Impostos de Locatários ou Usuários sobre as Taxas de Anúncio, e o
          valor deve ser remitido à respectiva Autoridade Fiscal. As leis podem
          variar de acordo com a jurisdição, porém pode ser solicitado o
          recolhimento e a remessa desses impostos como uma porcentagem das
          Taxas de Anúncio estabelecidas pelos Usuários, um valor estabelecido
          por dia, ou outras variações (“Impostos de Ocupação”).
        </p>
        <p>
          19.4. Em algumas jurisdições, a Oneer pode decidir, a seu critério
          exclusivo, facilitar o recolhimento e a remessa de Impostos de
          Ocupação de, ou em nome de Locatários ou Usuários, de acordo com estes
          Termos (“Recolhimento e Remessa”) caso tal jurisdição atribua à Oneer
          ou ao Locação uma obrigação fiscal de recolhimento e remessa. Em
          qualquer jurisdição na qual decidamos facilitar o Recolhimento e
          Remessa direta, você instrui e autoriza a Oneer a recolher Impostos de
          Ocupação de Locatários em nome do Locador no momento em que as Taxas
          de Anúncio forem recolhidas, e remeter tais Impostos de Ocupação à
          Autoridade Fiscal. O valor dos Impostos de Ocupação, se houver,
          recolhidos e remitidos pela Oneer, caso aplicável, estará visível e
          separadamente estabelecido para os Locatários e Usuários em seus
          respectivos documentos da transação. Quando a Oneer facilita o
          Recolhimento e o Repasse, os Usuários não têm permissão para recolher
          nenhum Imposto de Ocupação sendo cobrado pela Oneer relativo às suas
          Acomodações naquela jurisdição.
        </p>
        <p>
          19.5. Você concorda que qualquer pedido ou causa de ação relacionada à
          facilitação por parte da Oneer do Recolhimento e Remessa de Impostos
          não se estenderá a qualquer fornecedor ou vendedor que venha a ser
          usado pela Oneer em relação à facilitação do Recolhimento e da
          Remessa, se aplicável. Os Locadores concordam que podemos solicitar
          valores adicionais a você caso os Impostos recolhidos e/ou remitidos
          sejam insuficientes para desonerá-lo por completo de suas obrigações
          perante a Autoridade Fiscal, e concordam que o seu recurso exclusivo
          para Impostos de Ocupação recolhidos é um reembolso dos Impostos de
          Ocupação recolhidos pela Oneer da Autoridade Fiscal aplicável de
          acordo com os procedimentos aplicáveis definidos pela Autoridade
          Fiscal.
        </p>
        <p>
          19.6. A Oneer se reserva ao direito, com aviso prévio aos Usuários, de
          cessar o Recolhimento e a Remessa em qualquer jurisdição, por qualquer
          motivo que determine que os Usuários e os Locatários sejam novamente
          os responsáveis exclusivos pelo recolhimento e/ou pela remessa de todo
          e qualquer Imposto de Ocupação que possa ser aplicável às Acomodações
          naquela jurisdição.
        </p>
        <br />
        <p>
          <strong>20. Atividades Proibidas</strong>
        </p>
        <p>
          20.1. Você é o único responsável pelo cumprimento de toda e qualquer
          lei, regra, regulamento e obrigação fiscal que possa ser aplicável ao
          seu uso da Plataforma Oneer. De acordo com seu uso da Plataforma
          Oneer, você não fará nem auxiliará nem possibilitará que outras
          pessoas:
        </p>
        <p>
           Violem ou contornem quaisquer leis ou regulamentos aplicáveis,
          contratos com terceiros, direitos de terceiros ou nossos Termos,
          Políticas ou Padrões;
        </p>
        <p>
           Utilizar a Plataforma Oneer ou o Conteúdo Coletivo para qualquer fim
          residencial ou para quaisquer outros fins que não sejam os
          expressamente autorizados por estes Termos ou de uma forma que sugira
          falsamente que a Oneer endosse, tenha parceria ou de outra forma que
          engane outras pessoas como sendo a sua afiliação com a Oneer;
        </p>
        <p>
           Copiar, armazenar ou ainda acessar ou utilizar qualquer informação,
          incluindo informações pessoalmente identificáveis sobre qualquer outro
          Membro, contida na Plataforma Oneer de qualquer forma que seja
          inconsistente com a Política de Privacidade da Oneer ou com estes
          Termos ou que de outro modo violem os direitos de privacidade de
          Membros e de terceiros;
        </p>
        <p>
           Utilizar a Plataforma Oneer de acordo com a distribuição de
          mensagens comerciais não solicitadas (“spam”);
        </p>
        <p>
           Oferecer, como um Locador, qualquer imóvel que não pertença
          pessoalmente a você ou para a qual você não tenha permissão para
          disponibilizar como uma propriedade comercial ou de outra forma por
          meio da Plataforma Oneer;
        </p>
        <p>
           Exceto se a Oneer expressamente permitir em contrário, reservar
          qualquer Anúncio se você não for de fato utilizar os Serviços de
          Locação você mesmo;
        </p>
        <p>
           Entrar em contato com outro Membro para qualquer fim que não fazer
          uma pergunta relacionada a uma reserva sua, a um Anúncio, ou ao uso do
          Membro da Plataforma Oneer, incluindo, mas não se limitando a, o
          recrutamento ou a solicitação de qualquer Membro para aderir aos
          serviços, a aplicativos ou a sites de terceiros sem aprovação prévia
          por escrito;
        </p>
        <p>
           Utilizar a Plataforma Oneer para solicitar, fazer ou aceitar uma
          reserva independentemente da Plataforma Oneer, para contornar as Taxas
          de Serviço ou por qualquer outra razão;
        </p>
        <p>
           Solicitar, aceitar ou fazer qualquer pagamento por Taxas de Anúncio
          fora da Plataforma Oneer ou da Wirecard Brazil S.A. Se você fizer
          isto, você reconhece e concorda que: (i) estaria em violação destes
          Termos; (ii) aceita todos os riscos e responsabilidade por tal
          pagamento, e (iii) manterá a Oneer isenta de qualquer responsabilidade
          por tal pagamento;
        </p>
        <p>
           Agir com atitude de discriminação ou assédio com qualquer pessoa com
          base em raça, nacionalidade de origem, religião, gênero, identidade de
          gênero, deficiência física ou mental, condição médica, estado civil,
          idade ou orientação sexual, ou de outra forma participar de um
          comportamento violento, prejudicial, abusivo ou desordeiro;
        </p>
        <p>
           Fazer mal-uso ou abusar de Anúncios ou serviços associados ao
          Programa Acomodações Abertas conforme determinado pela Oneer o seu
          exclusivo critério;
        </p>
        <p>
           Usar, exibir, imitar ou reproduzir a Plataforma Oneer ou o Conteúdo
          Coletivo, ou qualquer elemento individual, dentro da Plataforma Oneer,
          o nome Oneer, qualquer marca comercial da Oneer, logotipo ou outras
          informações de propriedade, ou o layout e design de qualquer página ou
          formulário contido em uma página na Plataforma Oneer, sem o
          consentimento expresso e por escrito da Oneer;
        </p>
        <p>
           Manchar ou prejudicar a marca Oneer de qualquer forma, inclusive por
          meio de utilização não autorizada do Conteúdo Coletivo, registrar e/ou
          usar a Oneer ou termos derivados em nomes de domínio, marcas
          registradas, marcas comerciais ou outros identificadores de origem, ou
          registrar e/ou usar nomes de domínios, marcas registradas, marcas
          comerciais ou outros identificadores de origem que imitem
          estreitamente ou sejam confusamente similares a domínios da Oneer,
          marcas registradas, slogans, campanhas promocionais ou Conteúdo
          Coletivo;
        </p>
        <p>
           Utilizar robôs, “spider”, “crawler” “scraper” ou outro tipo de meio
          ou processo automático para acessar, coletar dados ou outro tipo de
          conteúdo de ou interagir com a Plataforma Oneer para qualquer fim;
        </p>
        <p>
           Ignorar, remover, desativar, prejudicar, decodificar, ou tentar
          contornar qualquer medida tecnológica que tenha sido implantada pela
          Oneer ou por qualquer dos provedores da Oneer ou qualquer outro
          terceiro para proteção da Plataforma Oneer;
        </p>
        <p>
           Tentar decifrar, descompilar, desmontar ou fazer engenharia reversa
          de qualquer software usado para fornecimento da Plataforma Oneer;
        </p>
        <p>
           Realizar qualquer ação que cause prejuízo ou afete negativamente, ou
          que possa causar prejuízo ou afetar negativamente o desempenho ou o
          devido funcionamento da Plataforma Oneer;
        </p>
        <p>
           Exportar, reexportar, importar ou transferir o Aplicativo, exceto na
          forma autorizada pela legislação dos Estados Unidos, pelas leis de
          controle de exportação de sua jurisdição e quaisquer outras leis
          aplicáveis; ou
        </p>
        <p>
           Violar ou infringir o direito de qualquer pessoa ou ainda causar
          dano a qualquer pessoa.
        </p>
        <p>
          20.2. Você reconhece que a Oneer não tem a obrigação de monitorar o
          acesso ou o uso da Plataforma Oneer por qualquer Membro ou revisar,
          desabilitar o acesso, ou editar o Conteúdo de qualquer Membro, mas tem
          o direito de fazê- lo para (i) operar, garantir e melhorar a
          Plataforma Oneer (incluindo, sem limitação, para fins de prevenção de
          fraude, avaliação de risco, investigação e atendimento ao cliente);
          (ii) garantir a conformidade dos Membros com estes Termos; (iii)
          cumprir com a lei aplicável ou com a decisão ou exigência de um
          tribunal, exigência legal ou outra agência administrativa ou órgão do
          governo; (iv) responder ao Conteúdo de Membro que determine como
          danoso ou questionável; ou (v) conforme determinado em contrário
          nestes Termos. Os Membros concordam em cooperar com a Oneer e
          auxiliá-la de boa-fé, e em fornecer à Oneer essas informações e
          realizar as medidas que foram solicitadas pela Oneer com relação a
          qualquer investigação feita pela Oneer ou por um representante da
          Oneer com relação ao uso ou abuso da Plataforma Oneer.
        </p>
        <p>
          20.3. Se você sentir que qualquer Membro com o qual você interaja,
          independentemente de ser online ou pessoalmente, agiu de forma
          inadequada, incluindo, mas não limitado a, qualquer um que (i)
          participe de comportamento ofensivo, violento ou sexualmente
          inadequado, (ii) você suspeite que roubou ou furtou você, ou (iii) se
          envolva em qualquer outra conduta perturbadora, você deve denunciar
          imediatamente essa pessoa às autoridades competentes e, em seguida,
          para a Oneer, contatando-nos com o posto ou delegacia de polícia e
          fornecendo-nos o número do boletim de ocorrência (se disponível). Você
          concorda que qualquer boletim de ocorrência que você fizer não nos
          obriga a tomar qualquer ação (além da exigida por lei, se houver).
        </p>
        <br />
        <p>
          <strong>21. Prazo e Rescisão, Suspensão e Outras Medidas</strong>
        </p>
        <p>
          21.1. Este Contrato entrará em vigor por um período de 30 dias, ao
          término desse período ele será renovado de maneira automática e
          contínua por prazos subsequentes de 30 dias até o momento em que você
          ou a Oneer rescindirem Contrato de acordo com esta disposição.
        </p>
        <p>
          21.2. Você pode rescindir este Contrato a qualquer momento nos
          enviando um e-mail. Se você cancelar a sua Conta Oneer como Locação,
          todas as Reservas confirmadas serão canceladas automaticamente e seus
          Locatários receberão um reembolso integral. Se você cancelar a sua
          Conta Oneer como Locatário, todas as Reservas confirmadas serão
          canceladas automaticamente e qualquer reembolso dependerá dos termos
          da política de cancelamento do Anúncio.
        </p>
        <p>
          21.3. A Oneer poderá rescindir este Contrato sem causa aparente, a
          qualquer momento, fornecendo a você uma notificação com 15 dias de
          antecedência via e-mail para o seu endereço de e-mail registrado.
        </p>
        <p>
          21.4. A Oneer pode imediatamente, sem aviso prévio, rescindir este
          Contrato e/ou parar de oferecer acesso à Plataforma Oneer se (i) você
          tiver violado materialmente suas obrigações conforme estes Termos, os
          Termos de Pagamento, nossas Políticas ou nossos Padrões, (ii) você
          tiver violado as leis, as regulamentações ou os direitos de terceiros
          aplicáveis, ou(iii) a Oneer acreditar, de boa-fé, que a referida ação
          seja razoavelmente necessária para proteger a segurança pessoal ou a
          propriedade da Oneer, de seus Membros, ou de terceiros (por exemplo,
          no caso de comportamento fraudulento por parte de um Membro).
        </p>
        <p>
          21.5. Além disso, a Oneer pode adotar quaisquer das seguintes medidas
          (i) para cumprir com a lei aplicável, ou com a decisão ou exigência de
          um tribunal, exigência da lei ou outro órgão governamental ou agência
          administrativa, ou se (ii) você tiver violado estes Termos, os Termos
          de Pagamento, ou nossas Políticas ou nossos Padrões, as leis, as
          regulamentações ou os direitos de terceiros aplicáveis, (iii) você
          tiver fornecido informações incompletas, desatualizadas, fraudulentas
          ou imprecisas na etapa de cadastro de sua Conta Oneer, processo de
          registro do Anúncio ou outro aí relacionado, (iv) você e/ou seus
          Anúncios ou Serviços de Locação a qualquer momento não atendam
          quaisquer critérios de elegibilidade ou qualidade, (v) você tiver
          repetidamente recebido Avaliações ou Comentários negativos ou se a
          Oneer ficar ciente de ou receber reclamações sobre seu desempenho ou
          conduta, (vi) você tiver cancelado repetidas vezes reservas
          confirmadas ou não respondido solicitações de reserva sem um motivo
          válido, ou (vii) a Oneer acreditar, de boa-fé, que essa medida é
          razoavelmente necessária para proteger a segurança pessoal ou da
          propriedade da Oneer, de seus Membros ou terceiros, ou evitar fraude
          ou outras atividades ilegais:
        </p>
        <p>
           Recusa à revelação, exclusão ou atraso em quaisquer Anúncios,
          Avaliações, Comentários ou outro Conteúdo de Membro;
        </p>
        <p> Cancelamento de quaisquer reservas confirmadas ou pendentes;</p>
        <p> Limitação de seu acesso ou uso da Plataforma Oneer;</p>
        <p>
           Revogação temporária ou permanente de qualquer situação especial
          relacionada à sua Conta Oneer;
        </p>
        <p>
           Suspensão temporária ou permanente, em caso de delitos recorrentes
          ou graves, da sua Conta Oneer e interrupção do acesso à Plataforma
          Oneer.
        </p>
        <p>
          21.6. Em caso de violações não materiais e conforme adequado, você
          será notificado de qualquer medida pretendida por parte da Oneer e
          terá oportunidade de resolver o problema à satisfação razoável da
          Oneer.
        </p>
        <p>
          21.7. Caso adotemos qualquer uma das medidas descritas acima (i)
          podemos reembolsar seus Locatários integralmente por toda e qualquer
          reserva que tenha sido cancelada, independentemente das políticas de
          cancelamento pré- existentes, e (ii) você não receberá qualquer
          pagamento por reservas confirmadas ou pendentes que tenham sido
          canceladas.
        </p>
        <p>
          21.8. Quando este Contrato for encerrado, você não terá direito a uma
          restauração da sua Conta Oneer ou qualquer um dos seus Conteúdos de
          membro. Se o seu acesso ou uso da Plataforma Oneer for limitado, ou a
          sua Conta Oneer for suspensa ou este Contrato for rescindido por nós,
          você não poderá registrar uma nova Conta Oneer ou acessar e utilizar a
          Plataforma Oneer, através da Conta Oneer de outro Membro.
        </p>
        <p>
          21.9. Se você ou nós rescindirmos este Contrato, as cláusulas destes
          Termos que devam sobreviver razoavelmente à rescisão do Contrato
          permanecerão em vigor.
        </p>
        <br />
        <p>
          <strong>22. Isenções</strong>
        </p>
        <p>
          22.1. Se você escolher utilizar a Plataforma Oneer, você o faz
          voluntariamente e a seu próprio risco. A Plataforma Oneer e o Conteúdo
          Coletivo são fornecidos “como estão”, sem garantia de qualquer
          natureza, expressa ou implícita.
        </p>
        <p>
          22.2. Você concorda que você teve toda oportunidade para considerar
          necessário investigar os Serviços da Oneer, as leis, as regras ou os
          regulamentos que podem ser aplicáveis aos seus Anúncios e/ou aos
          Serviços de Locação que você receber e que nos quais você não confie
          em qualquer declaração da lei ou fato divulgado pela Oneer com relação
          a um Anúncio.
        </p>
        <p>
          22.3. Se optarmos por realizar essas verificações de identidade ou
          histórico com qualquer Membro, conforme permitido pela legislação
          aplicável, nós renunciamos às garantias de qualquer tipo, expressas ou
          implícitas, de que estes controles identificarão atos ilícitos prévios
          por parte de um usuário ou garantirão que o Membro não se envolverá em
          atos ilícitos no futuro.
        </p>
        <p>
          22.4. Você concorda que os Serviços de Locação ou o Serviço de
          Pagamento podem trazer riscos inerentes, e que ao participar desses
          serviços você opta por assumir esses riscos voluntariamente. Por
          exemplo, alguns Serviços de Locação podem trazer riscos de doença,
          lesão corporal, deficiência ou morte, e você tem a liberdade e o
          direito de assumir esses riscos ao optar por participar desses
          Serviços de Locação. Você assume toda a responsabilidade por suas
          escolhas antes, durante e depois da participação em um Serviço de
          Locação ou no Serviço de Pagamento em Grupo. Caso você leve um menor
          de idade como um Locatário adicional, você é o único responsável pela
          supervisão do referido menor de idade durante todo o período de seu
          Serviço de Locação e na medida permitida por lei, você se compromete a
          manter a Oneer isenta de responsabilidade e de prejuízos relativos à
          responsabilidade e reclamações que provenham de qualquer modo de dano,
          morte, perda ou lesão que ocorra ao menor de idade durante o Serviço
          de Locação ou de qualquer forma relacionado ao Serviço de Locação.
        </p>
        <br />
        <p>
          <strong>
            23. Recusa de Garantia, Limitação de Responsabilidade e Indenização
          </strong>
        </p>
        <p>
          23.1. O usuário expressamente concorda e está ciente de que: (i) a
          utilização do serviço será sob inteira responsabilidade do usuário. O
          serviço é fornecido ao usuário na forma em que está disponível. a
          Oneer não oferece garantias outras além das expressamente
          estabelecidas nestes termos; e (ii) a Oneer não pode garantir que: (a)
          o serviço à disposição atenderá às necessidades do usuário; (b) o
          serviço será prestado de forma ininterrupta, tempestiva; (c) que a
          qualidade de quaisquer outras ofertas, informações ou outro material
          acessado, obtido, disponibilizado ou prestado ao usuário em conexão ao
          serviço atenderá às expectativas; (d) que melhoramentos ou inovações
          serão implementados; e (e) os serviços de reservas solicitados por
          meio do uso da plataforma serão confiáveis, pontuais e adequados.
        </p>
        <p>
          23.2. O usuário expressamente concorda e está ciente de que a Oneer
          não terá qualquer responsabilidade, seja contratual ou
          extracontratual, por quaisquer danos patrimoniais ou morais,
          incluindo, sem limitação, danos por lucros cessantes ou de informações
          ou outras perdas intangíveis resultantes do: (a) uso ou incapacidade
          de usar o serviço; (b) quebras de segurança e acesso não autorizado às
          transmissões ou informações do usuário, bem como da alteração destas;
          (c) orientações ou condutas de terceiros sobre o serviço; e (d) por
          motivos de força maior ou caso fortuito, atos praticados pelo próprio
          usuário e atos praticados por ou sob a responsabilidade de terceiros.
        </p>
        <p>
          23.3. O Usuário está ciente de que os dados do Membro divulgados pela
          Oneer, incluindo, mas não se limitando a isto, seu nome completo,
          endereço e comodidades disponíveis no local ofertado foram fornecidos
          pelo próprio Membro.
        </p>
        <p>
          23.4. A isenção acima se aplica na máxima extensão permitida por lei.
          Você pode ter outros direitos estatutários. Entretanto, a duração das
          garantias exigidas estatutariamente, se aplicável, serão limitadas à
          máxima extensão permitida por lei.
        </p>
        <p>
          23.5. Você reconhece e concorda que, à máxima extensão permitida por
          lei, permanece sob sua responsabilidade todo o risco proveniente de
          seu acesso e uso da Plataforma Oneer e Conteúdo Coletivo, sua
          publicação ou reserva de qualquer Anúncio por meio da Plataforma
          Oneer, sua estadia em qualquer imóvel comercial, participação em
          qualquer Evento ou uso de qualquer outro Serviço de Locação,
          participação no Serviço de Pagamento em Grupo, ou qualquer outra
          interação que você tenha com outros Membros, independentemente de
          pessoalmente ou online. Nem a Oneer nem qualquer outra parte envolvida
          na criação, produção ou disponibilização da Plataforma Oneer ou do
          Conteúdo Coletivo será responsável por quaisquer danos incidentais,
          especiais, exemplares ou consequenciais, incluindo lucros cessantes,
          perda de dados ou perda de fundo de comércio, interrupção do serviço,
          danos em computador ou falha no sistema, ou do custo substituição de
          produtos ou serviços, ou por quaisquer danos pessoais ou corporais ou
          morais ou angústia emocional decorrente de ou relacionados a estas
          condições, do uso ou da impossibilidade de utilizar o site, o
          aplicativo, os serviços ou o conteúdo coletivo, decorrentes de
          quaisquer comunicações com (i) estes Termos, (ii) de seu uso de ou de
          sua incapacidade de utilizar a Plataforma Oneer ou o Conteúdo
          Coletivo, (iii) de quaisquer comunicações, interações ou atendimentos
          a outros Membros ou outras pessoas com quem você se comunique,
          interaja ou se encontre como resultado do uso da Plataforma, ou (iv)
          de sua publicação ou reserva de qualquer Anúncio, incluindo a
          disponibilização ou o uso de Serviços de Locação do Anúncio, seja
          baseado em garantia, contrato, ato ilícito (incluindo negligência),
          responsabilidade sobre produto ou qualquer outra teoria jurídica, e
          mesmo que a Oneer tenha sido informada ou não da possibilidade de tais
          danos, mesmo que uma solução limitada aqui contida não tenha cumprido
          a sua finalidade essencial. Exceto pelas nossas obrigações de
          pagamento para com os Usuários aplicáveis conforme disposição destes
          termos ou um pedido de pagamento aprovado sob a Garantia ao Locação da
          Oneer, em hipótese alguma a responsabilidade agregada da Oneer
          decorrente de ou relacionada a estes termos e sua utilização da
          Plataforma Oneer, incluindo, sem limitação, de sua publicação ou
          reserva de quaisquer Anúncios através da Plataforma Oneer, ou da
          utilização ou da incapacidade de utilização a Plataforma Oneer, ou do
          Conteúdo Coletivo ou sua, e de acordo com qualquer Acomodação,
          Experiência, Evento, outro Serviço de Locação ou o Serviço de
          Pagamento em Grupo, ou interações com quaisquer outros Membros,
          excederão o valor pago ou devido pelas reservas através da Plataforma
          Oneer como um Locatário no período de 12 (doze) meses anteriores ao
          evento gerador da responsabilidade, ou se você for um Locação, o valor
          pago pela Oneer para você no período de 12 (doze) meses anteriores ao
          evento gerador da responsabilidade, ou R$ 100,00 (cem reais), caso
          nenhum de tais pagamentos tenha sido feito, conforme aplicável. As
          limitações de danos estabelecidas acima são elementos fundamentais da
          base do acordo entre você e a Oneer. Algumas jurisdições não permitem
          a exclusão ou limitação de responsabilidade por danos consequenciais
          ou incidentais, sendo assim a limitação acima pode não se aplicar a
          você. Se você residir fora do Brasil, isso não afeta a
          responsabilidade da Oneer por morte ou lesão pessoal proveniente de
          negligência, nem por representação fraudulenta, representação falsa
          quanto a um assunto crucial ou qualquer outra responsabilidade que não
          possa ser excluída ou limitada sob a lei aplicável.
        </p>
        <p>
          23.6. A Oneer é responsável, sob disposições estatutárias, por atos de
          intenção e negligência grosseira cometidos por nós, por nossos
          representantes legais, conselheiros ou outros representantes
          indiretos. O mesmo se aplica ao comprometimento com garantias ou
          qualquer outra responsabilidade rigorosa ou em caso de uma lesão
          culposa à vida, a um membro ou à saúde. A Oneer é responsável por
          quaisquer violações de obrigações contratuais por negligência nossa,
          de nossos representantes legais, conselheiros ou outros representantes
          indiretos. Obrigações contratuais essenciais são tarefas da Oneer em
          cujo devido atendimento você confie regularmente e deva confiar para a
          devida execução do contrato, mas o valor deve ser limitado
          considerando o dano tipicamente previsível ocorrido. Exclui-se
          qualquer responsabilidade adicional da Oneer.
        </p>
        <p>
          23.7. Você concorda em isentar, defender (a critério da Oneer),
          indenizar, e manter a Oneer e suas afiliadas e subsidiárias,
          incluindo, mas não limitado a, a Wirecard Brazil S.A., e seus
          diretores, conselheiros, funcionários e agentes livres de danos por e
          contra qualquer reclamação, responsabilidade, danos, prejuízos e
          despesas, incluindo, sem limitação, taxas de contabilidade e jurídicas
          razoáveis, provenientes de ou de qualquer modo relacionadas com (i)
          sua violação destes Termos ou de nossas Políticas ou Padrões, (ii) seu
          uso indevido da Plataforma Oneer ou de quaisquer Serviços da Oneer,
          (iii) sua interação com qualquer Membro, estadia em uma Acomodação,
          participação em uma Experiência, um Evento ou outro Serviço de
          Locação, participação no Serviço de Pagamento em Grupo, incluindo, sem
          limitações e danos, prejuízos e lesões (independentemente de serem
          compensatórias, diretas, incidentais, consequenciais ou outras) de
          qualquer natureza relacionados a ou, como resultado da referida
          interação, estadia, participação ou uso, (iv) Recolhimento e Remessa,
          por parte da Oneer, de Impostos de Ocupação, ou (v) sua violação de
          quaisquer leis, regulamentações ou direitos de terceiros.{" "}
        </p>
        <br />
        <p>
          <strong>24. Resolução de Litígios</strong>
        </p>
        <p>
          <strong>24.1 Feedback</strong>
        </p>
        <p>
          24.1.1. Apreciamos e o incentivamos a fornecer feedback, comentários e
          sugestões de melhorias para a Plataforma Oneer (“Feedback”). Você pode
          nos enviar um Feedback por e-mail ou através da seção de “Contato” da
          Plataforma Oneer, ou por qualquer outro meio de comunicação. Qualquer
          Feedback que você nos enviar será considerado não confidencial e não
          será considerado como propriedade sua. Ao nos enviar um Feedback, você
          nos dá uma licença mundial, não exclusiva, livre de royalties,
          irrevogável, sublicenciável e perpétua para utilizar e publicar essas
          ideias e esses materiais para qualquer propósito, sem lhe pagar por
          isso.
        </p>
        <p>
          <strong>24.2. Legislação Aplicável</strong>
        </p>
        <p>
          24.2.1. Estes Termos e o relacionamento entre o Membro e a Oneer serão
          regidos pelas leis da República Federativa do Brasil. Fica eleito o
          foro da Comarca da Capital do Estado do Paraná como sendo o único
          competente para dirimir quaisquer litígios e/ou demandas que venham a
          envolver as partes em relação ao uso e acesso de seu site e
          Aplicativo. O Membro e a Oneer concordam em submeter-se à competência
          única e exclusiva dos tribunais localizados no Brasil.
        </p>
        <p>
          <strong>24.3. Notificações</strong>
        </p>
        <p>
          24.3.1. Todas as notificações, intimações, ofícios ou qualquer outra
          forma oficial de cientificação da Oneer deverão ocorrer em seu
          endereço de sede, descrito no preâmbulo dos Termos.
        </p>
        <p>
          <strong>24.4. Renúncia de Direitos</strong>
        </p>
        <p>
          24.4.2. Caso a Oneer deixe de exercer ou executar qualquer direito ou
          dispositivo destes Termos, isto não será caracterizado como uma
          renúncia a tal direito ou dispositivo nem constituirá novação. Se
          qualquer dispositivo dos Termos for considerado inválido por um
          tribunal competente, as partes, todavia concordam que o restante dos
          Termos deverá ser interpretado de tal forma a refletir a intenção
          original, e os outros dispositivos dos Termos permanecerão em pleno
          vigor e efeito.
        </p>
        <p>
          <strong>24.5. Cessão de Direitos</strong>
        </p>
        <p>
          24.5.1. O Membro não poderá ceder tampouco transferir estes Termos,
          total ou parcialmente, sem prévia aprovação por escrito da Oneer. O
          Membro concede sua aprovação para que a Oneer ceda e transfira estes
          Termos total ou parcialmente, inclusive: (i) para sua controladora,
          subsidiária ou afiliada; (ii) um adquirente das participações
          acionárias, negócios ou bens da Oneer; ou (iii) para um sucessor em
          razão de qualquer operação societária. Não existe joint- venture,
          sociedade, emprego ou relação de representação entre o Passageiro, a
          Oneer ou quaisquer Membros como resultado do contrato entre você e a
          Oneer ou pelo uso dos Serviços.
        </p>
        <p>
          <strong>24.6. Indivisibilidade</strong>
        </p>
        <p>
          24.6.1. Caso qualquer disposição destes Termos seja tida como ilegal,
          inválida ou inexequível total ou parcialmente, por qualquer
          legislação, essa disposição ou parte dela será, naquela medida,
          considerada como não existente para os efeitos destes Termos, mas a
          legalidade, validade e exequibilidade das demais disposições contidas
          nestes Termos não serão afetadas. Nesse caso, as partes substituirão a
          disposição ilegal, inválida ou inexequível, ou parte dela, por outra
          que seja legal, válida e exequível e que, na máxima medida possível,
          tenha efeito similar à disposição tida como ilegal, inválida ou
          inexequível para fins de conteúdo e finalidade dos presentes Termos.
        </p>
        <p>
          24.6.2. Estes Termos constituem a totalidade do acordo e entendimento
          das partes sobre este assunto e substituem e prevalecem sobre todos os
          entendimentos e compromissos anteriores sobre este assunto. Nestes
          Termos, as palavras “inclusive” e “inclui” significam “incluindo, sem
          limitação”.
        </p>
        <br />
        <p>
          <strong>25. Propriedade Intelectual</strong>
        </p>
        <p>
          25.1. As marcas, nomes, logotipos, nomes de domínio e demais sinais
          distintivos, bem como todo e qualquer conteúdo, desenho, arte ou
          layout publicado no Aplicativo e o próprio Serviço, são de propriedade
          exclusiva da Oneer.
        </p>
        <p>
          25.2. São vedados quaisquer atos ou contribuições tendentes à
          descompilação, engenharia reversa, modificação das características,
          ampliação, alteração, mesclagem ou incorporação em quaisquer outros
          programas ou sistemas do Aplicativo ou Serviço. Enfim, toda e qualquer
          forma de reprodução do Aplicativo ou Serviço, total ou parcial,
          permanente, temporária ou provisória, de forma gratuita ou onerosa,
          sob quaisquer modalidades, formas ou títulos é expressamente vedada.
        </p>
        <br />
        <p>
          <strong>26. Disposições Gerais</strong>
        </p>
        <p>
          26.1 Exceto na medida em que puderem ser complementados por padrões,
          diretrizes, políticas, termos e condições adicionais, estes Termos
          constituem o Acordo integral entre a Oneer e você com relação ao
          objeto aqui referido, e prevalece sobre todo e qualquer acordo oral ou
          escrito anterior entre a Oneer e você com relação ao acesso e ao uso
          da Plataforma Oneer.
        </p>
        <p>
          26.2. Não existe nenhuma joint venture, sociedade, vínculo
          empregatício ou relacionamento de agenciamento entre você e a Oneer
          nem como resultado deste Contrato nem pelo seu uso da Plataforma
          Oneer, sendo certo que o membro é livre para aceitar ou recusar
          reservas a partir do aplicativo, bem como para cessar a sua utilização
          a qualquer momento, ao seu livre e exclusivo critério.&nbsp;
        </p>
        <p>
          26.3. A Oneer não possui um portfólio de imóveis próprios, prestando
          exclusivamente os serviços de licenciamento e intermediação voltados à
          facilitação da contratação de serviço de reservas perante um membro
          cadastrado no aplicativo.
        </p>
        <p>
          26.4. Estes Termos não são e não serão destinados a conferir quaisquer
          direitos ou recursos a qualquer pessoa que não sejam as partes.
        </p>
        <p>
          26.5. Caso qualquer disposição destes Termos seja considerada inválida
          ou inexequível, a referida disposição será removida e não afetará a
          validade e a efetividade das disposições remanescentes aqui
          constantes.
        </p>
        <p>
          26.6. A não cobrança por parte da Oneer de qualquer direito ou
          disposição destes Termos não constituirá uma renúncia de tal direito
          ou disposição exceto se reconhecido e acordado por nós, por escrito.
          Exceto quando expressamente estabelecido nestes Termos, o exercício
          por qualquer das partes de qualquer medida judicial sob estes Termos,
          será sem prejuízo de outras medidas ao abrigo destes Termos ou de
          outra forma permitido por lei.
        </p>
        <p>
          26.7. Você não pode atribuir, transferir ou delegar este Contrato e
          seus direitos e suas obrigações aqui adquiridas sem o consentimento
          prévio por escrito da Oneer. A Oneer pode, sem restrições, atribuir,
          transferir ou delegar este Contrato e quaisquer direitos e obrigações
          aqui adquiridas, a seu exclusivo critério, desde que seja emitido
          aviso prévio de 30 dias. Seu direito de rescindir este Contrato a
          qualquer momento permanece inalterado.
        </p>
        <p>
          26.8. Exceto se especificado em contrário, qualquer notificação ou
          outras comunicações com Membros permitidas ou solicitadas por este
          Contrato serão fornecidas eletronicamente e enviadas pela Oneer por
          e-mail, por notificação pela Plataforma Oneer ou por serviço de
          mensagem (incluindo SMS e WeChat).
        </p>
        <p>
          26.9. Em caso de dúvidas sobre estes Termos, entre em contato conosco
          por oneerapp@gmail.com.
        </p>
        <br />
        <p>
          <strong> 27. Aceitação dos Termos e Condições de Uso</strong>
        </p>
        <p>
          27.1. Ao se cadastrar e utilizar continuamente os serviços, o membro
          estará declarando ter lido, entendido e aceito os termos. Caso, a
          qualquer tempo, o membro não concorde com os termos, deverá cessar
          imediatamente a utilização do aplicativo e desinstalá-lo de seu
          aparelho.
        </p>
        <p>
          27.2. Termos Adicionais poderão se aplicar a determinados Serviços,
          tais como condições para categorias específicas, planos, eventos,
          regulamentos, atividades, promoções em particular ou ações de
          recompensas, e esses Termos Adicionais serão divulgados em relação aos
          respectivos Serviços (“Termos Adicionais”). Os Termos Adicionais são
          complementares e considerados parte integrante destes Termos para os
          efeitos dos respectivos Serviços. Os Termos Adicionais prevalecerão
          sobre estes Termos em caso de conflito somente com relação àquilo que
          forem específicos.
        </p>
        <div className="space"></div>
      </div>
    </div>
  </>
);

export default TermsOfUsePrincipal;
