import React, { useEffect, useState } from "react";
import Title from "./components/Title";
import MakePaymentContent from "./components/MakePaymentContent";

function MakeThePaymentPrincipal({ data }) {
  return (
    <>
      <div className="clearfix"></div>
      <Title
        title={"Efetuar Pagamento da Reserva"}
        breadcrumbs={`Informe os dados do seu Cartão de Crédito`}
      ></Title>
      <div className="container">
        <div className="row sticky-wrapper">
          <div className="row">
            <div className="col-md-12 text-center">
              {data && data.reservation_code && (
                <MakePaymentContent data={data}></MakePaymentContent>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default MakeThePaymentPrincipal;
