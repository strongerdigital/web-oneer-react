import React from "react";
import Title from "./components/Title";
import Register from "./components/Auth/Register";

// import { Container } from './styles';

export default function MyAccountPrincipal() {
  return (
    <>
      <div className="clearfix"></div>
      <Title title={"Minha Conta"}></Title>
      <Register></Register>
    </>
  );
}
