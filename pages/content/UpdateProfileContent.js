import React, { useEffect, useState } from "react";
import Title from "./components/Title";
import { connect } from "react-redux";
import { getLogged } from "../../providers/services/user";
import * as AppActions from "../../store/actions/app";
import ProfileActions from "./components/ProfileActions";
import ProfileMenu from "./components/ProfileMenu";
import RegisterForm from "./components/Auth/RegisterForm";
// import { Container } from './styles';

function UpdateProfilePrincipal() {
  const [logged, setLogged] = useState(false);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        setLogged(user);
      }
    });
  }, []);
  return logged.id ? (
    <>
      <div className="clearfix"></div>
      <Title title={"Minha conta"}></Title>
      <div className="container">
        <div className="row sticky-wrapper">
          <ProfileActions></ProfileActions>
          <div className="row">
            <ProfileMenu></ProfileMenu>
            <div className="col-md-9">
              <RegisterForm logged={logged}></RegisterForm>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : (
    <></>
  );
}

export default UpdateProfilePrincipal;
