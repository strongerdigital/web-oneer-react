import React, { useEffect, useState } from "react";
import Title from "./components/Title";
import { connect } from "react-redux";
import { getLogged } from "../../providers/services/user";
import * as AppActions from "../../store/actions/app";
import ProfileActions from "./components/ProfileActions";
import ProfileMenu from "./components/ProfileMenu";
import NewPlaceForm from "./components/NewPlaceForm";
// import { Container } from './styles';

function NewPlacePrincipal({ data }) {
  const [logged, setLogged] = useState(false);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        setLogged(user);
      }
    });
  }, []);
  return (
    <>
      <div className="clearfix"></div>
      <Title title={"Novo anúncio"}></Title>

      <div className="container">
        <div className="row sticky-wrapper">
          <ProfileActions></ProfileActions>
          <div className="row">
            <ProfileMenu></ProfileMenu>
            <div className="col-md-9">
              {data && (
                <NewPlaceForm
                  data={data}
                  logged={logged ? logged : {}}
                ></NewPlaceForm>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default NewPlacePrincipal;
