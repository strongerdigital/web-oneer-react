import React, { useEffect, useState } from "react";
import Title from "./components/Title";
import { connect } from "react-redux";
import { getLogged, getMyLocations } from "../../providers/services/user";
import * as AppActions from "../../store/actions/app";
import ProfileActions from "./components/ProfileActions";
import ProfileMenu from "./components/ProfileMenu";
import MyLocationsContent from "./components/MyLocationsContent";
// import { Container } from './styles';

function MyLocationsPrincipal() {
  const [locations, setLocations] = useState([]);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        getMyLocations(user.id).then(items => {
          setLocations(items);
        });
      }
    });
  }, []);
  return locations ? (
    <>
      <div className="clearfix"></div>
      <Title title={"Minhas reservas"}></Title>
      <div className="container">
        <div className="row sticky-wrapper">
          <ProfileActions></ProfileActions>
          <div className="row">
            <ProfileMenu></ProfileMenu>
            <div className="col-md-9">
              <MyLocationsContent data={locations}></MyLocationsContent>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : (
    <></>
  );
}

export default MyLocationsPrincipal;
