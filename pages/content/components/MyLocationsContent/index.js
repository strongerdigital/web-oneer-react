import React from "react";
import MyLocationItem from "../MyLocationItem";

// import { Container } from './styles';

export default function MyLocationsContent({ data }) {
  return (
    <>
      <br />
      <div className="row">
        <div className="col-md-3 text-center">
          <h4>Espaço</h4>
        </div>
        <div className="col-md-4 text-center">
          <h4>Reserva</h4>
        </div>
        <div className="col-md-3 ">
          <h4>Status</h4>
        </div>
        <div className="col-md-2 "></div>
      </div>
      <br />
      <div className="row">
        {data &&
          data.map(location => (
            <MyLocationItem
              location={location}
              key={location.id}
            ></MyLocationItem>
          ))}
      </div>
    </>
  );
}
