import React from "react";

// import { Container } from './styles';

export default function ProfileMenu() {
  const handleLogout = async e => {
    e.preventDefault();
    if (confirm("Deseja sair da sua conta?")) {
      await localStorage.removeItem("@oneer:user");
      location.href = "minhaconta";
    }
  };
  return (
    <div className="menu-box">
      <div className="col-md-3">
        <div className="col-md-12">
          <div className="menu-box-item">
            <a title="Meu Perfil" href="/editar-perfil">
              <span className="text-block">Meu Perfil</span>
            </a>
          </div>
        </div>
        <div className="col-md-12">
          <div className="menu-box-item">
            <a title="Minhas Reservas" href="/minhas-reservas">
              <span className="text-block">Minhas Reservas</span>
            </a>
          </div>
        </div>
        <div className="col-md-12">
          <div className="menu-box-item">
            <a title="Meus Anúncios" href="/meus-anuncios">
              <span className="text-block">Meus Anúncios</span>
            </a>
          </div>
        </div>
        <div className="col-md-12">
          <div className="menu-box-item">
            <a title="Meus Favoritos" href="/meus-favoritos">
              <span className="text-block">Meus Favoritos</span>
            </a>
          </div>
        </div>
        <div className="col-md-12">
          <div className="menu-box-item">
            <a title="Plano Workpass" href="/plano-workpass">
              <span className="text-block">Plano Workpass</span>
            </a>
          </div>
        </div>
        <div className="col-md-12">
          <div className="menu-box-item">
            <a
              title="Central de Ajuda"
              href="https://oneer.app/central-de-ajuda"
            >
              <span className="text-block">Ajuda</span>
            </a>
          </div>
        </div>
        <div className="col-md-12">
          <div className="menu-box-item">
            <a title="Central de Ajuda" href="#" onClick={handleLogout}>
              <span className="text-block">Sair</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
