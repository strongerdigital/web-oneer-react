import React, { useEffect } from "react";

// import { Container } from './styles';

export default function MyPlacesContent({ places }) {
  return (
    <div className="row">
      {places &&
        places.map(item => (
          <div className="col-md-12" key={item.id}>
            <div className="col-md-3">{item.title}</div>
            <div className="col-md-3">
              {item.street},{item.number} <br />
              {item.neighborhood} <br />
              {item.city.slug} / {item.city.state.slug} <br />
            </div>
            <div className="col-md-3"></div>
            <div className="col-md-2"></div>
            <div className="col-md-1">
              <a className="" href={`editaranuncio?id=${item.id}`}>
                <i className="fa fa-pencil"></i>
              </a>
            </div>
            <hr className="col-md-12"></hr>
          </div>
        ))}
    </div>
  );
}
