import React, { useState, useEffect } from "react";

import api from "../../../../providers/api";

import DatePicker from "react-datepicker";
import PlaceAvailability from "../PlaceAvailability";
import {
  confirmationLocation,
  getLogged
} from "../../../../providers/services/user";
// import { Container } from './styles';

export default function PlaceSidebar({ place }) {
  const [formData, setFormData] = useState({});
  const [availability, setAvailability] = useState(null);
  const [availabilityError, setAvailabilityError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loggedUser, setLoggedUser] = useState(false);
  const verifyDisponibity = async () => {
    if (!loading) {
      if (
        formData.occupants &&
        formData.date_init &&
        formData.date_end &&
        formData.hour_init &&
        formData.hour_end
      ) {
        setLoading(true);
        setAvailability(null);
        const request = await api.post(
          `advertisings/${place.id}/availability`,
          formData
        );
        const { data } = request;
        setLoading(false);
        if (data.success) {
          setAvailability(request.data);
        } else {
          setAvailabilityError(request.data);
        }
      } else {
        setAvailabilityError({
          error: "Favor preencher todos os campos"
        });
      }
    }
  };
  const makeReservation = async () => {
    getLogged().then(async user => {
      const confirmation = await confirmationLocation(
        {
          ...formData,
          advertising_id: place.id,
          price: availability.price,
          price_total: availability.price_total
        },
        user
      );
      if (confirmation.id) {
        location.href = "/location?advertising_location=" + confirmation.id;
      }
    });
  };

  useEffect(() => {
    getLogged().then(user => {
      setLoggedUser(user);
    });
  }, []);
  return (
    <div className="col-lg-4 col-md-5">
      <div className="sidebar sticky right">
        <div className="title-search-home">Verifique a disponibilidade</div>
        <div className="form-search-work right-work">
          <div className="row with-forms">
            <div className="col-md-6">
              <h5 className="title-search-work">Data checkin</h5>

              <DatePicker
                selected={formData.date_init}
                onChange={date => {
                  setFormData({
                    ...formData,
                    date_init: date
                  });
                }}
              ></DatePicker>
            </div>
            <div className="col-md-6">
              <h5 className="title-search-work">Hora</h5>
              <DatePicker
                selected={formData.hour_init}
                onChange={hour_init =>
                  setFormData({
                    ...formData,
                    hour_init
                  })
                }
                showTimeSelect
                showTimeSelectOnly
                timeIntervals={15}
                timeCaption="Time"
                dateFormat="h:mm aa"
              />
            </div>

            <div className="col-md-6">
              <h5 className="title-search-work">Data checkout</h5>
              <DatePicker
                selected={formData.date_end}
                onChange={date => {
                  setFormData({
                    ...formData,
                    date_end: date
                  });
                }}
              ></DatePicker>
            </div>
            <div className="col-md-6">
              <h5 className="title-search-work">Hora</h5>
              <DatePicker
                selected={formData.hour_end}
                onChange={hour_end =>
                  setFormData({
                    ...formData,
                    hour_end
                  })
                }
                showTimeSelect
                showTimeSelectOnly
                timeIntervals={15}
                timeCaption="Time"
                dateFormat="h:mm aa"
              />
            </div>
            <h5 className="title-search-work">Quantas pessoas?</h5>
            <div className="col-md-12">
              <input
                type="number"
                value={formData.occupants || ""}
                onChange={e => {
                  setFormData({
                    ...formData,
                    occupants: e.target.value
                  });
                }}
              ></input>
              <button
                className="button-search-work"
                onClick={verifyDisponibity}
              >
                {loading ? (
                  <i className="fa fa-spinner fa-spin"></i>
                ) : (
                  "Verificar"
                )}
              </button>
              {availability && (
                <PlaceAvailability
                  loggedUser={loggedUser}
                  availability={availability}
                  makeReservation={makeReservation}
                ></PlaceAvailability>
              )}
              {availabilityError && <h5>{availabilityError.error}</h5>}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
