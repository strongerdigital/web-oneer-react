import React, { useState } from "react";
import MaskedInput from "react-text-mask";
import { cpfMask, dateMask, cvcMask, cardMask } from "../../../../consts/masks";
// import { Container } from './styles';

export default function CreditCard({ handleMakePayment }) {
  const [formData, setFormData] = useState({});

  const handleSubmit = e => {
    e.preventDefault();
    handleMakePayment(formData);
  };
  return (
    <form
      className="simple_form new_user"
      id="new_user"
      onSubmit={handleSubmit}
    >
      <div className="row text-left">
        <div className="col-xs-12 col-sm-6 col-md-6 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Nome do Titular do Cartão
            </label>
            <input
              className="string required form-control field-contact"
              autoFocus="autofocus"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.card_name || ""}
              onChange={e => {
                setFormData({ ...formData, card_name: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-3 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr>CPF do titular
            </label>
            <MaskedInput
              guide={false}
              mask={cpfMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.identifier || ""}
              onChange={e => {
                setFormData({ ...formData, identifier: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-3 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Data de nascimento
            </label>
            <MaskedInput
              guide={false}
              mask={dateMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.birthdate || ""}
              onChange={e => {
                setFormData({ ...formData, birthdate: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Número do cartão
            </label>
            <MaskedInput
              guide={false}
              mask={cardMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.card_number || ""}
              onChange={e => {
                setFormData({ ...formData, card_number: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-3 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Mês - validade
            </label>
            <select
              className="string required form-control field-contact"
              required="required"
              a-required="true"
              value={formData.card_month || ""}
              onChange={e => {
                setFormData({ ...formData, card_month: e.target.value });
              }}
            >
              <option value="">Selecione</option>
              <option value="01">01 - Janeiro</option>
              <option value="02">02 - Fevereiro</option>
              <option value="03">03 - Março</option>
              <option value="04">04 - Abril</option>
              <option value="05">05 - Maio</option>
              <option value="06">06 - Junho</option>
              <option value="07">07 - Julho</option>
              <option value="08">08 - Agosto</option>
              <option value="09">09 - Setembro</option>
              <option value="10">10 - Outubro</option>
              <option value="11">11 - Novembro</option>
              <option value="12">12 - Dezembro</option>
            </select>
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-3 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Ano - validade
            </label>
            <select
              className="string required form-control field-contact"
              required="required"
              a-required="true"
              value={formData.card_year || ""}
              onChange={e => {
                setFormData({ ...formData, card_year: e.target.value });
              }}
            >
              <option value="">Selecione</option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
              <option value="2022">2022</option>
              <option value="2023">2023</option>
              <option value="2024">2024</option>
              <option value="2025">2025</option>
              <option value="2026">2026</option>
              <option value="2027">2027</option>
              <option value="2028">2028</option>
              <option value="2029">2029</option>
              <option value="2030">2030</option>
              <option value="2031">2031</option>
              <option value="2032">2032</option>
              <option value="2033">2033</option>
            </select>
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-2 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> CVC
            </label>
            <MaskedInput
              guide={false}
              mask={cvcMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              value={formData.name || ""}
              onChange={e => {
                setFormData({ ...formData, name: e.target.value });
              }}
            />
          </div>
        </div>
      </div>
      <br />
      <br />
      <button className="button">Finalizar Pagamento</button>
      <br />
      <br />
      <br />
      <br />
    </form>
  );
}
