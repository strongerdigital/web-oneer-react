import React from "react";
import Link from "next/link";
import env from "../../../../consts/env";

// import { Container } from './styles';
/* istanbul ignore next*/
export default function PlaceListItem({ place }) {
  const ShowPhoto = () => {
    if (place && place.photos && place.photos.length > 0) {
      return <img src={env.publicURL + place.photos[0].image} alt="" />;
    } else {
      return <img src="/static/images/listing-01.jpg" alt="" />;
    }
  };
  return place ? (
    <div className="listing-item">
      <Link
        href={{
          pathname: "/espaco",
          query: { slug: place.slug }
        }}
        as={`/espaco?slug=${place.slug}`}
      >
        <a className="listing-img-container">
          <div className="listing-badges">
            <span className="featured">Novo</span>
          </div>
          <div className="listing-carousel">
            <div>
              <ShowPhoto></ShowPhoto>
            </div>
          </div>
        </a>
      </Link>
      <div className="listing-content">
        <div className="listing-title">
          <h4>{place.title}</h4>
          <a
            href={place.map_url}
            target="_blank"
            className="listing-address popup-gmaps"
          >
            <i className="fa fa-map-marker"></i>
            {place ** place.street} - {place && place.neighborhood} <br />
            {place && place.city && place.city.slug}/
            {place && place.city && place.city.state && place.city.state.name}{" "}
            <br />
          </a>
          <Link
            href={{
              pathname: "/espaco",
              query: { slug: place.slug }
            }}
            as={`/espaco?slug=${place.slug}`}
          >
            <a href="/espaco" className="details button border">
              Detalhes
            </a>
          </Link>
        </div>
        <ul className="listing-details">
          <li>{place && place.accessories.map(item => `${item.name}  /`)}</li>
        </ul>

        <div className="listing-footer">
          <div className="col-md-4">
            <a href="#">
              <i className="fa fa-star"></i>5
            </a>
          </div>
          <div className="col-md-4">
            <a href="#">
              <i className="fa fa-eye"></i> 12
            </a>
          </div>

          <div className="col-md-4">
            <a href="#">
              <i className="fa fa-dollar"></i> R${place.price},00/h
            </a>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : (
    <></>
  );
}
