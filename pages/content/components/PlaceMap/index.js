import React from "react";

// import { Container } from './styles';

export default function PlaceMap({ place }) {
  let street = "";
  let city = "";

  if (place && place.city && place.city.slug) {
    street = place.street.replace(/ /g, "+");
    city = place.city.slug.replace(/ /g, "+");
  }

  return place && place.city ? (
    <iframe
      src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyBQDS5bsu4GQSzEooibZ3nVNbjR67bkneI&q=${place.number}+${street},+${city},+${place.city.state.slug}`}
      width="100%"
      height="450"
      frameBorder="0"
      allowFullScreen
    ></iframe>
  ) : (
    <></>
  );
}
