import React from "react";
import env from "../../../../consts/env";
// import { Container } from './styles';

export default function Photos({ data }) {
  const photos = data ? data.data : [];
  return (
    <div className="container">
      <div className="row margin-bottom-50">
        <div
          id="carouselExampleSlidesOnly"
          className="carousel slide"
          data-ride="carousel"
        >
          <div className="carousel-inner">
            {photos &&
              photos.length > 0 &&
              photos.map(item => (
                <div className="carousel-item active" key={item.id}>
                  <img
                    height={200}
                    className="d-block w-100"
                    src={env.publicURL + item.image}
                    alt=" slide"
                  />
                </div>
              ))}
          </div>
        </div>
      </div>
    </div>
  );
}
