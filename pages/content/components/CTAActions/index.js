import React, { useState, useEffect } from "react";
import { getLogged } from "../../../../providers/services/user";

function CTAActions() {
  const [logged, setLogged] = useState(false);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        setLogged(user);
      }
    });
  }, []);
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4">
          <a
            href="/espacos"
            className="img-box small"
            data-background-image="/static/images/popular-location-02.jpg"
          >
            <div className="listing-badges">
              <span className="featured">Conheça já</span>
            </div>

            <div className="img-box-content visible">
              <h4>Lugares para reuniões conforme o seu gosto </h4>
              <span>55 espaços</span>
            </div>
          </a>
        </div>

        <div className="col-md-8">
          <a
            href={logged ? "/novoanuncio" : "/minhaconta"}
            className="img-box large"
            data-background-image="/static/images/popular-location-01.jpg"
          >
            <div className="img-box-content visible">
              <h4>Ganhe dinheiro com a sua sala de reunião</h4>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}

export default CTAActions;
