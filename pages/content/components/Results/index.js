import React from "react";
import PlaceListItem from "../PlaceListItem";

// import { Container } from './styles';

export default function Results({ data }) {
  return (
    <div className="col-md-9">
      <div className="row margin-bottom-15">
        <div className="col-md-6">
          {data && data.length > 0 && (
            <div className="sort-by">
              <label>Filtre por: &nbsp; &nbsp;</label>

              <div className="sort-by-select">
                <select
                  data-placeholder="Default order"
                  className="chosen-select-no-single"
                >
                  <option>Todos os espaços</option>
                  <option>Workpass</option>
                  <option>Menor preço hora</option>
                  <option>Menor preço diária</option>
                  <option>Maior preço hora</option>
                  <option>Maior preço diária</option>
                </select>
              </div>
            </div>
          )}
        </div>
      </div>

      <div className="listings-container list-layout listing-work">
        {data &&
          data.map(item => (
            <PlaceListItem place={item} key={item.id}></PlaceListItem>
          ))}
      </div>
      {/* 
      <div className="pagination-container margin-top-20">
        <nav className="pagination">
          <ul>
            <li>
              <a href="#" className="current-page">
                1
              </a>
            </li>
            <li>
              <a href="#">2</a>
            </li>
            <li>
              <a href="#">3</a>
            </li>
            <li className="blank">...</li>
            <li>
              <a href="#">22</a>
            </li>
          </ul>
        </nav>

        <nav className="pagination-next-prev">
          <ul>
            <li>
              <a href="#" className="prev">
                Anterior
              </a>
            </li>
            <li>
              <a href="#" className="next">
                Próximo
              </a>
            </li>
          </ul>
        </nav>
      </div> */}
    </div>
  );
}
