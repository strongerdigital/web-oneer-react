import React from "react";

// import { Container } from './styles';

export default function CTAIcons() {
  return (
    <>
      <section
        className="fullwidth margin-top-105"
        data-background-color="#f7f7f7"
      >
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-sm-6">
              <div className="icon-box-1">
                <div className="icon-container">
                  <i className="im im-icon-Assistant"></i>
                </div>

                <p>Ideal para quem está começando. </p>

                <p> Cômodo para quem atende em mais de um local. </p>

                <p> Barato para quem gosta de investir em si. </p>

                <p> Rentável para quem tem espaços vagos. </p>

                <p> Arrojado para quem quer explorar a cidade.</p>
              </div>
            </div>

            <div className="col-md-4 col-sm-6">
              <div className="icon-box-1">
                <div className="icon-container">
                  <i className="im im-icon-Building"></i>
                </div>

                <p>
                  Você escolhe o local, o dia e o horário, reserva e pode usar.{" "}
                </p>

                <p> Para quem tem pacientes em outras cidades.</p>

                <p>
                  {" "}
                  Para os que atenderão em uma cidade nova ou em uma região
                  desconhecida.{" "}
                </p>

                <p> Para os que gostam de uma agenda flexível. </p>

                <p> E para todos que gostam de curtir com a sua galera.</p>
              </div>
            </div>

            <div className="col-md-4 col-sm-6">
              <div className="icon-box-1">
                <div className="icon-container">
                  <i className="im im-icon-Dollar"></i>
                </div>

                <p>Dinheiro no bolso de quem tem um espaço. </p>

                <p> Experiência nova para quem acabou de chegar. </p>

                <p>
                  {" "}
                  Conforto de estar perto de casa, do cliente, da faculdade.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
