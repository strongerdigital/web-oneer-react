import React from "react";
import NumberFormat from "react-number-format";

const Currency = ({ number }) => (
  <NumberFormat
    value={parseFloat(number).toFixed(2)}
    displayType={"text"}
    prefix={"R$ "}
    fixedDecimalScale={true}
    decimalSeparator={"."}
    renderText={value => <>{value}</>}
  />
);

export default Currency;
