import React from "react";

// import { Container } from './styles';

export default function Title({ title, breadcrumbs }) {
  return (
    <div className="title-content titlebar">
      <div id="titlebar">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h2>{title}</h2>
              {breadcrumbs && <span>{breadcrumbs}</span>}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
