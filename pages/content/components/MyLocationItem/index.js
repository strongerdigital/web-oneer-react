import React from "react";
import env from "../../../../consts/env";
import { format } from "date-fns";
import Link from "next/link";
import Currency from "../Currency";
// import { Container } from './styles';
/* istanbul ignore next*/
export default function MyLocationItem({ location }) {
  const advertising = location ? location.advertising : {};
  const ShowPhoto = () => {
    if (
      advertising &&
      advertising &&
      advertising.photos &&
      advertising.photos.length > 0
    ) {
      return (
        <img
          src={env.publicURL + advertising.photos[0].image}
          alt=""
          className="col-md-12"
        />
      );
    } else {
      return (
        <img src="/static/images/listing-01.jpg" alt="" className="col-md-12" />
      );
    }
  };

  const ShowStatus = () => {
    let status = "";
    switch (location.payment_status) {
      case "waiting":
        status = "AGUARDANDO PAGAMENTO";
        break;
      case "IN_ANALYSIS":
        status = "EM ANÁLISE";
        break;
      case "AUTHORIZED":
        status = "PAGO";
        break;

      default:
        break;
    }

    return status;
  };

  return location ? (
    <div className="col-md-12">
      <div className="col-md-3">
        <ShowPhoto></ShowPhoto>
        <Link
          href={{
            pathname: "/espaco",
            query: { slug: advertising.slug }
          }}
          as={`/espaco?slug=${advertising.slug}`}
        >
          <a> {advertising.title}</a>
        </Link>
      </div>

      <div className="col-md-4 text-center">
        de {format(location.date_init, "DD/MM/YYYY")} - {location.hour_init}
        <br />
        até {format(location.date_end, "DD/MM/YYYY")} - {location.hour_end}
        <br />
        Ocupantes: {location.occupants} pessoa
        {location.occupants != 1 ? "s" : ""}
        <br />
        <br />
        Valor: <Currency number={location.price_total}></Currency>
      </div>
      <div className="col-md-3">
        <small>
          <ShowStatus />
        </small>
      </div>
      <div className="col-md-2">
        <br />
        {location.payment_status === "waiting" && (
          <a
            className="button"
            href={`/finalizar-pagamento?advertising_location=${location.id}`}
          >
            Pagar
          </a>
        )}
        <br />
        <br />
        <a className="" href={`location?advertising_location=${location.id}`}>
          Detalhes
        </a>
        <br />
        {location.payment_status === "waiting" && <a className="">Cancelar</a>}
      </div>

      <hr className="col-md-12"></hr>
    </div>
  ) : (
    <></>
  );
}
