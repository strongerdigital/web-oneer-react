import React, { useState } from "react";

// import { Container } from './styles';

export default function Banner() {
  const [search, setSearch] = useState({});
  return search ? (
    <div className={"banner-home"}>
      <div className="">
        <div className="container">
          <div className="row">
            <div className="col-md-6 search-work">
              <div className="title-search-home">ENCONTRE O LOCAL PERFEITO</div>
              <div className="form-search-work">
                <div className="row with-forms">
                  <h5 className="title-search-work">Qual o local?</h5>
                  <div className="col-md-12">
                    <form action="espacos">
                      <input
                        name="busca"
                        type="text"
                        className="ico-01"
                        placeholder="Local/endereço/bairro"
                        value={search.place || ""}
                        onChange={e => {
                          setSearch({
                            place: e.target.value
                          });
                        }}
                      />
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <></>
  );
}
