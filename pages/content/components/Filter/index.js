import React, { useState } from "react";

// import { Container } from './styles';

export default function Filter({ data, nocategory }) {
  const [showFilter, setShowFilter] = useState(false);
  const [filter, setFilter] = useState({});
  return data ? (
    <div className="col-md-3">
      <div className="sidebar sticky right">
        <div className="widget margin-bottom-40">
          <h3 className="margin-top-0 margin-bottom-35">Filtre por</h3>

          {!nocategory && (
            <div className="row with-forms">
              <div className="col-md-12">
                <select
                  data-placeholder="Categoria"
                  className="chosen-select-no-single"
                  value={filter.category_id || ""}
                  onChange={e => {
                    setFilter({
                      ...filter,
                      category_id: e.target.value
                    });
                  }}
                >
                  <option vlaue="">Categoria</option>
                  {data &&
                    data.categories &&
                    data.categories.map(item => (
                      <option key={item.id} value={item.id}>
                        {item.title}
                      </option>
                    ))}
                </select>
              </div>
            </div>
          )}
          <div className="row with-forms">
            <div className="col-md-12">
              <select
                data-placeholder="Quantas pessoas?"
                className="chosen-select-no-single"
                value={filter.occupants || ""}
                onChange={e => {
                  setFilter({
                    ...filter,
                    occupants: e.target.value
                  });
                }}
              >
                <option value="">Quantas pessoas?</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
            </div>
          </div>

          <div className="row with-forms">
            <div className="col-md-12">
              <select data-placeholder="Hora Checkin" className="chosen-select">
                <option>Hora Checkin</option>
                <option>05:00</option>
                <option>06:00</option>
                <option>07:00</option>
                <option>08:00</option>
                <option>09:00</option>
                <option>10:00</option>
                <option>11:00</option>
                <option>12:00</option>
                <option>13:00</option>
                <option>14:00</option>
                <option>15:00</option>
                <option>16:00</option>
                <option>17:00</option>
                <option>18:00</option>
                <option>19:00</option>
              </select>
            </div>
          </div>

          <div className="row with-forms">
            <div className="col-md-12">
              <select
                data-placeholder="Data Checkin"
                className="chosen-select-no-single"
              >
                <option label="blank"></option>
                <option>Data Checkin</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
          </div>

          <div className="row with-forms">
            <div className="col-md-12">
              <select data-placeholder="Hora Checkin" className="chosen-select">
                <option>Data Checkout</option>
                <option>05:00</option>
                <option>06:00</option>
                <option>07:00</option>
                <option>08:00</option>
                <option>09:00</option>
                <option>10:00</option>
                <option>11:00</option>
                <option>12:00</option>
                <option>13:00</option>
                <option>14:00</option>
                <option>15:00</option>
                <option>16:00</option>
                <option>17:00</option>
                <option>18:00</option>
                <option>19:00</option>
              </select>
            </div>
          </div>

          <div className="row with-forms">
            <div className="col-md-12">
              <select data-placeholder="Hora Checkin" className="chosen-select">
                <option>Hora Checkout</option>
                <option>05:00</option>
                <option>06:00</option>
                <option>07:00</option>
                <option>08:00</option>
                <option>09:00</option>
                <option>10:00</option>
                <option>11:00</option>
                <option>12:00</option>
                <option>13:00</option>
                <option>14:00</option>
                <option>15:00</option>
                <option>16:00</option>
                <option>17:00</option>
                <option>18:00</option>
                <option>19:00</option>
              </select>
            </div>
          </div>

          <button
            href="#"
            className="more-search-options-trigger margin-bottom-10 margin-top-30"
            data-open-title="Comodidades"
            data-close-title="Comodidades"
            onClick={() => {
              setShowFilter(!showFilter);
            }}
          ></button>

          {showFilter && (
            <div className="relative">
              <div className="checkboxes one-in-row margin-bottom-10">
                <input
                  id="check-all"
                  type="checkbox"
                  name="check"
                  onChange={() => {
                    console.log("asdasd");
                  }}
                />
                <label htmlFor="check-all">Marcar todas as comodidades</label>
                <br />

                {data &&
                  data.accessories &&
                  data.accessories.map(item => (
                    <div key={item.id}>
                      <input
                        id={`check-${item.id}`}
                        type="checkbox"
                        name="accessories"
                        value={item.id}
                      />
                      <label htmlFor={`check-${item.id}`}>{item.name}</label>
                      <br />
                    </div>
                  ))}
              </div>
            </div>
          )}

          <button
            className="button fullwidth margin-top-30"
            onClick={() => {
              console.log(filter);
            }}
          >
            Filtrar agora
          </button>
        </div>
      </div>
    </div>
  ) : (
    <></>
  );
}
