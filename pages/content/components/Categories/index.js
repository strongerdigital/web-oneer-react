import React, { useEffect } from "react";
import Link from "next/link";
import env from "../../../../consts/env";
export default function Categories({ data }) {
  return (
    <section
      className="fullwidth border-top margin-top-55 margin-bottom-0 padding-top-60 padding-bottom-65"
      data-background-color="#ffffff"
    >
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3 className="headline margin-bottom-25 margin-top-65">
              Veja as categorias que possuem espaços comerciais para reserva
            </h3>
          </div>
          <div className="col-md-12">
            {data &&
              data.map(item => (
                <div className="item col-md-2" key={item.id}>
                  <Link
                    href={{
                      pathname: "/categoria",
                      query: { slug: item.slug }
                    }}
                    as={`/categoria?slug=${item.slug}`}
                  >
                    <a>
                      <img
                        src={
                          item.image
                            ? env.filesURL + item.image
                            : "/static/images/nophoto.png"
                        }
                        alt=""
                      />
                      <h4> {item.title}</h4>
                    </a>
                  </Link>
                </div>
              ))}
          </div>
        </div>
      </div>
    </section>
  );
}
