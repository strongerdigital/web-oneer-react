import React from "react";
import Currency from "../Currency";
import env from "../../../../consts/env";
// import { Container } from './styles';
/* istanbul ignore next*/
export default function Featured({ places }) {
  const ShowPhoto = ({ place }) => {
    if (place && place.photos && place.photos.length > 0) {
      return <img src={env.publicURL + place.photos[0].image} alt="" />;
    } else {
      return <img src="/static/images/listing-01.jpg" alt="" />;
    }
  };
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h3 className="headline margin-bottom-25 margin-top-65">
            Os mais visitados
          </h3>
        </div>

        <div className="col-md-12">
          <div className="">
            <div className="">
              {places &&
                places.map(place => (
                  <div className="col-md-4" key={place.id}>
                    <a
                      href={`/espaco?slug=${place.slug}`}
                      className="listing-img-container"
                    >
                      <div className="listing-img-content">
                        <span className="listing-price">
                          <i>
                            <Currency number={place.price}></Currency> / Hora
                          </i>
                        </span>
                      </div>

                      <div className="listing-carousel">
                        <div>
                          <ShowPhoto place={place}></ShowPhoto>
                        </div>
                      </div>
                    </a>

                    <div className="listing-content">
                      <div className="listing-title">
                        <h4>
                          <a href="/espaco">{place.title}</a>
                        </h4>
                        <a
                          href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                          className="listing-address popup-gmaps"
                        >
                          <i className="fa fa-map-marker"></i>
                          {place.street} | {place.city.slug}/
                          {place.city.state.slug}
                        </a>
                      </div>

                      <div className="listing-footer">
                        <i className="fa fa-star fa-2x"></i>
                        <i className="fa fa-star fa-2x"></i>
                        <i className="fa fa-star fa-2x"></i>
                        <i className="fa fa-star fa-2x"></i>
                        <i className="fa fa-star fa-2x"></i>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
