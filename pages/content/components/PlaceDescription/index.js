import React from "react";
import Currency from "../Currency";

export default function PlaceDescription({ place }) {
  return place ? (
    <div className="col-lg-8 col-md-7">
      <div className="property-description">
        <ul className="property-main-features">
          <li>
            Hora:
            <span>
              <Currency number={place.price}></Currency>
            </span>
          </li>
          <li>
            Dia:
            <span>
              <Currency number={place.price * 8}></Currency>
            </span>
          </li>
          <li>
            Semana
            <span>
              <Currency number={place.price * 40}></Currency>
            </span>
          </li>
          <li>
            Mês
            <span>
              <Currency number={place.price * 880}></Currency>
            </span>
          </li>
        </ul>
        <hr />
        <ul className="property-main-features">
          <li>
            Cidade: <span>{place && place.city && place.city.slug}</span>
          </li>
          <li>
            Endereço:
            <span>
              {place && place.street}, {place && place.neighborhood} -{" "}
              {place && place.number}
            </span>
          </li>
        </ul>

        <h3 className="desc-headline">Descrição</h3>

        <p>{place.description}</p>

        <h3 className="desc-headline">Comodidades</h3>
        <ul className="property-features checkboxes margin-top-0">
          {place &&
            place.accessories &&
            place.accessories.map(item => <li key={item.id}>{item.name}</li>)}
        </ul>
        <br />
        <br />
      </div>
    </div>
  ) : (
    <></>
  );
}
