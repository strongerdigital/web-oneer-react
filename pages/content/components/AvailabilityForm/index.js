import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";

// import { Container } from './styles';

export default function AvailabilityForm({
  item,
  removeAvailability,
  setAvailabilitiyData
}) {
  const [itemData, setItemData] = useState(item);

  useEffect(() => {
    setAvailabilitiyData(itemData);
  }, [itemData]);
  return item ? (
    <>
      <div className="row">
        <div className="col-md-3">
          <label>Dia da semana</label>
          <select
            value={item.weekday || ""}
            onChange={e => {
              setItemData({
                ...itemData,
                weekday: e.target.value
              });
            }}
          >
            <option value="">Selecione o dia</option>
            <option value="1">Segunda-Feira</option>
            <option value="2">Terça-Feira</option>
            <option value="3">Quarta-Feira</option>
            <option value="4">Quinta-Feira</option>
            <option value="5">Sexta-Feira</option>
            <option value="6">Sábado</option>
            <option value="7">Domingo</option>
          </select>
        </div>
        <div className="col-md-3">
          <label>Início</label>
          <DatePicker
            selected={item.hour_init}
            onChange={date => {
              setItemData({
                ...itemData,
                hour_init: date
              });
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Time"
            dateFormat="h:mm aa"
          ></DatePicker>
        </div>
        <div className="col-md-3">
          <label>Fim</label>
          <DatePicker
            selected={item.hour_end}
            onChange={date => {
              setItemData({
                ...itemData,
                hour_end: date
              });
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Time"
            dateFormat="h:mm aa"
          ></DatePicker>
        </div>
        <div className="col-md-3">
          <a className="btn btn-danger" onClick={removeAvailability}>
            <i className="fa fa-trash" />
          </a>
        </div>
      </div>
      <hr className="col-md-12"></hr>
    </>
  ) : (
    <></>
  );
}
