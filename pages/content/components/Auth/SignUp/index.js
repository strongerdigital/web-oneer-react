import React from "react";
import Title from "../../Title";
import NewUserForm from "../NewUserForm";

// import { Container } from './styles';

export default function SignUp() {
  return (
    <>
      <div className="clearfix"></div>
      <Title
        title={"Criar conta Oneer"}
        breadcrumbs={"Registre-se agora mesmo! É rápido e fácil!"}
      ></Title>
      <div className="container">
        <NewUserForm />
      </div>
    </>
  );
}
