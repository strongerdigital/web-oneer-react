import React, { useState, useEffect } from "react";
import MaskedInput from "react-text-mask";
import { getCep } from "../../../../../providers/services/app";
import { cepMask, phoneMask, mobileMask } from "../../../../../consts/masks";
import { makeSignup, updateUser } from "../../../../../providers/services/user";

export default function NewUserForm() {
  const [formData, setFormData] = useState({});
  const handleSubmit = async e => {
    e.preventDefault();
    handleSignup();
  };

  const handleSignup = async () => {
    const response = await makeSignup(formData);
    if (!response.error) {
      await localStorage.setItem("@oneer:user", JSON.stringify(response.user));
      location.href = "/profile";
    }
  };

  const handleCep = async () => {
    if (formData.zipcode && formData.zipcode.length === 9) {
      const response = await getCep(formData.zipcode);
      if (response.cidade) {
        setFormData({
          ...formData,
          neigborhood: response.bairro,
          city: response.cidade,
          street: response.logradouro,
          state: response.uf,
          city_id: response.city_id
        });
      }
    }
  };
  return (
    <form
      className="simple_form new_user"
      id="new_user"
      onSubmit={handleSubmit}
    >
      <div className="row">
        <div className="col-xs-12 col-sm-6 col-md-6 form-group">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Preencha o seu nome
            </label>
            <input
              className="string required form-control field-contact"
              autoFocus="autofocus"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.name || ""}
              onChange={e => {
                setFormData({ ...formData, name: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-6 form-group">
          <div className="input email required user_email">
            <label className="email required" htmlFor="user_email">
              <abbr title="obrigatório">*</abbr> Qual o seu e-mail?
            </label>
            <input
              className="string email required form-control field-contact"
              required="required"
              aria-required="true"
              type="email"
              value=""
              name="user[email]"
              id="user_email"
              value={formData.email || ""}
              onChange={e => {
                setFormData({ ...formData, email: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 form-group">
          <div className="input string required user_identifier">
            <label className="string required" htmlFor="user_identifier">
              <abbr title="obrigatório">*</abbr> Informe seu CPF/CNPJ
            </label>
            <input
              className="string required form-control field-contact"
              maxLength="18"
              type="number"
              required="required"
              aria-required="true"
              size="18"
              name="user[identifier]"
              id="user_identifier"
              value={formData.identifier || ""}
              onChange={e => {
                setFormData({ ...formData, identifier: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 form-group">
          <div className="input tel optional user_phone">
            <label className="tel optional" htmlFor="user_phone">
              E o seu telefone?
            </label>
            <MaskedInput
              guide={false}
              mask={phoneMask}
              className="string tel optional form-control phonefix field-contact"
              type="tel"
              name="user[phone]"
              id="user_phone"
              value={formData.phone || ""}
              onChange={e => {
                setFormData({ ...formData, phone: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-4 form-group">
          <div className="input string optional user_mobile">
            <label className="string optional" htmlFor="user_mobile">
              E o seu celular?
            </label>
            <MaskedInput
              guide={false}
              mask={mobileMask}
              className="string optional form-control phone field-contact"
              type="text"
              name="user[mobile]"
              id="user_mobile"
              value={formData.mobile || ""}
              onChange={e => {
                setFormData({ ...formData, mobile: e.target.value });
              }}
            />
          </div>
        </div>
      </div>
      <div className="minspace"></div>
      <div className="alert alert-info">
        <strong>* Campos obrigatórios</strong>
      </div>
      <div className="space"></div>
      <div className="row">
        <div className="col-xs-12 col-sm-6 col-md-3 form-group">
          <div className="input string required user_zipcode">
            <label className="string required" htmlFor="user_zipcode">
              <abbr title="obrigatório">*</abbr> Informe seu CEP
            </label>
            <MaskedInput
              guide={false}
              mask={cepMask}
              className="string required form-control zipcode"
              type="tel"
              required="required"
              aria-required="true"
              name="user[zipcode]"
              id="user_zipcode"
              onBlur={handleCep}
              value={formData.zipcode || ""}
              onChange={e => {
                setFormData({ ...formData, zipcode: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-8 col-md-9 form-group">
          <div className="input string required user_street">
            <label className="string required" htmlFor="user_street">
              <abbr title="obrigatório">*</abbr> Informe seu Endereço
            </label>
            <input
              className="string required form-control"
              required="required"
              aria-required="true"
              type="text"
              name="user[street]"
              id="user_street"
              value={formData.street || ""}
              onChange={e => {
                setFormData({ ...formData, street: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-5 form-group">
          <div className="input string required user_neigborhood">
            <label className="string required" htmlFor="user_neigborhood">
              <abbr title="obrigatório">*</abbr> Informe seu Bairro
            </label>
            <input
              className="string required form-control"
              required="required"
              aria-required="true"
              type="text"
              name="user[neigborhood]"
              id="user_neigborhood"
              value={formData.neigborhood || ""}
              onChange={e => {
                setFormData({ ...formData, neigborhood: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-7 col-md-4 form-group">
          <div className="input string required user_city">
            <label className="string required" htmlFor="user_city">
              <abbr title="obrigatório">*</abbr> Informe sua Cidade
            </label>
            <input
              className="string required form-control"
              required="required"
              aria-required="true"
              type="text"
              name="user[city]"
              id="user_city"
              value={formData.city || ""}
              onChange={e => {
                setFormData({ ...formData, city: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-5 col-md-3 form-group">
          <div className="input string required user_state">
            <label className="string required" htmlFor="user_state">
              <abbr title="obrigatório">*</abbr> Informe seu Estado
            </label>
            <input
              className="string required form-control"
              required="required"
              aria-required="true"
              type="text"
              name="user[state]"
              id="user_state"
              value={formData.state || ""}
              onChange={e => {
                setFormData({ ...formData, state: e.target.value });
              }}
            />
          </div>
        </div>
      </div>
      <div className="gap"></div>

      <div className="row">
        <div className="col-xs-12 col-sm-6 col-md-6 form-group">
          <div className="input password required user_password">
            <label className="password required" htmlFor="user_password">
              <abbr title="obrigatório">*</abbr> Digite uma Senha
            </label>
            <input
              className="password required form-control field-contact"
              required="required"
              aria-required="true"
              type="password"
              name="user[password]"
              id="user_password"
              value={formData.password || ""}
              onChange={e => {
                setFormData({ ...formData, password: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-6 form-group">
          <div className="input password required user_password_confirmation">
            <label
              className="password required"
              htmlFor="user_password_confirmation"
            >
              <abbr title="obrigatório">*</abbr> Confirme a Senha
            </label>
            <input
              className="password required form-control field-contact"
              required="required"
              aria-required="true"
              type="password"
              name="user[password_confirmation]"
              id="user_password_confirmation"
              value={formData.password_confirmation || ""}
              onChange={e => {
                setFormData({
                  ...formData,
                  password_confirmation: e.target.value
                });
              }}
            />
          </div>
        </div>
      </div>

      <div className="gap"></div>

      <div className="checkbox">
        <div className="input boolean required user_accept_terms">
          <input value="0" type="hidden" name="user[accept_terms]" />
          <label
            className="boolean required checkbox"
            htmlFor="user_accept_terms"
          >
            <input
              className="boolean required"
              required="required"
              aria-required="true"
              type="checkbox"
              value="1"
              name="user[accept_terms]"
              id="user_accept_terms"
              value={formData.accpet_terms || ""}
              onChange={e => {
                setFormData({ ...formData, accpet_terms: e.target.value });
              }}
            />
            <abbr title="obrigatório">*</abbr> Ao iniciar a sessão você concorda
            com os{" "}
            <a
              title="Termos de Uso e Política de Privacidade"
              href="/termos-de-uso"
            >
              Termos de Serviço e nossa Política de Privacidade
            </a>
          </label>
        </div>
      </div>
      <div className="gap"></div>

      <span className="text-block">
        Atenção! Ao concluir o seu cadastro, acesse o e-mail informado no
        formulário de registro e confirme a sua conta.
      </span>
      <div className="gap"></div>
      <div className="hidden-xs">
        <div className="pull-right">
          <input
            type="submit"
            name="commit"
            value={"Concluir cadastro"}
            className="btn btn-login btn-featured"
            data-disable-with={"Concluir cadastro"}
          />
        </div>
      </div>
      <div className="hidden-sm hidden-md hidden-lg">
        <div className="row">
          <div className="form-group col-xs-12">
            <a
              title="Voltar para a Página Inicial"
              className="btn btn-danger btn-featured btn-block"
              href="/"
            >
              ← Voltar
            </a>
          </div>
          <div className="form-group col-xs-12">
            <input
              type="submit"
              name="commit"
              value="Concluir cadastro"
              className="btn btn-login btn-featured btn-block"
              data-disable-with="Concluir cadastro"
            />
          </div>
        </div>
      </div>
    </form>
  );
}
