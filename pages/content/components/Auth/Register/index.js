import React, { useState } from "react";
import { makeLogin } from "../../../../../providers/services/user";

export default function Register() {
  const [formData, setFormData] = useState({});
  const handleLogin = async e => {
    e.preventDefault();
    const request = await makeLogin(formData);
    if (request.user) {
      await localStorage.setItem("@oneer:user", JSON.stringify(request.user));
      location.href = "/profile";
    } else {
      alert("Favor conferir suas credenciais");
    }
  };
  return (
    <div className="container">
      <h3 className="text-center">
        Faça seu login ou registre-se agora mesmo!
      </h3>
      <div className="space"></div>
      <div className="row">
        <div className="col-xs-12 col-sm-6 col-md-6">
          <h3>Acesse sua conta</h3>
          <div className="gap"></div>
          <p>
            Bem vindo(a), para acessar sua conta, por favor informe seu endereço
            de
            <u>e-mail</u> e <u>senha de acesso</u>.
          </p>
          <div className="gap"></div>
          <form
            className="simple_form form-vertical"
            onSubmit={handleLogin}
            acceptCharset="UTF-8"
          >
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-10">
                <div className="form-group">
                  <div className="input email required user_email">
                    <label className="email required" htmlFor="user_email">
                      <abbr title="obrigatório">*</abbr> Informe seu usuário:
                    </label>
                    <input
                      className="string email required form-control field-contact"
                      autoFocus="autofocus"
                      required="required"
                      aria-required="true"
                      type="email"
                      value={formData.email || ""}
                      onChange={e => {
                        setFormData({
                          ...formData,
                          email: e.target.value
                        });
                      }}
                      id="user_email"
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="input password required user_password">
                    <label
                      className="password required"
                      htmlFor="user_password"
                    >
                      <abbr title="obrigatório">*</abbr> Senha de acesso:
                      <a
                        title="Recuperar senha de usuário"
                        href="/usuario/password/new"
                      >
                        Esqueci minha senha!
                      </a>
                    </label>
                    <input
                      className="password required form-control field-contact"
                      required="required"
                      aria-required="true"
                      type="password"
                      id="user_password"
                      value={formData.password || ""}
                      onChange={e => {
                        setFormData({
                          ...formData,
                          password: e.target.value
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="min-space"></div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-6 col-md-6">
                <div>
                  <div className="input boolean optional user_remember_me">
                    <input value="0" type="hidden" name="user[remember_me]" />
                  </div>
                </div>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-12">
                <div className="minspace"></div>
                <input
                  type="submit"
                  name="commit"
                  value="Acessar minha conta"
                  className="btn btn btn-login btn-featured"
                  data-disable-with="Acessar minha conta"
                />
              </div>
            </div>
          </form>
          <div className="space"></div>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-6">
          <h3>Ainda não é registrado?</h3>
          <div className="gap"></div>
          <p>
            Se você ainda não possui cadastro, clique no botão abaixo e preencha
            seus dados. É rápido e fácil.
          </p>
          <hr />
          <p>
            <a
              title="Criar conta de Acesso"
              className="btn btn-login btn-featured"
              href="/sign_up"
            >
              Criar minha conta agora
            </a>
          </p>
          <hr />
          <p>
            <i className="icon icon-lock icon-sm"></i>&nbsp; O formulário a
            seguir irá solicitar somente dados básicos para seu cadastro.
          </p>
        </div>
      </div>
      <div className="space"></div>
    </div>
  );
}
