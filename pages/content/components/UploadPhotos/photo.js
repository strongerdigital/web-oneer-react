import React from "react";

// import { Container } from './styles';

export default function Upload({ photo, removePhoto }) {
  return (
    <div className="thumbnailHolder">
      <button className="remove" onClick={removePhoto}>
        X
      </button>
      {photo && photo.image && (
        <img src={photo.image} className="thumbnail" alt="thumbanil" />
      )}
    </div>
  );
}
