import React, { useCallback, useState } from "react";
import { useDropzone } from "react-dropzone";
import Photo from "./photo";
export default function UploadPhotos({ handlePhotos }) {
  const [photos, setPhotos] = useState([]);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const onDrop = useCallback(
    acceptedFiles => {
      acceptedFiles.forEach(file => {
        var reader = new FileReader();
        reader.onprogress = () => setLoading(true);
        reader.onloadend = function(file) {
          const binaryStr = reader.result;
          photos.push({ image: binaryStr, file: acceptedFiles });
          setPhotos(photos);
          setCount(photos.length);
          handlePhotos(photos);
          setLoading(false);
        };
        reader.readAsDataURL(file);
      });
    },
    [handlePhotos, photos]
  );
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  const removePhoto = useCallback(
    key => {
      photos.splice(key, 1);
      setCount(photos.length);
      setPhotos(photos);
      handlePhotos(photos);
    },
    [handlePhotos, photos]
  );
  return (
    <div className="col-lg-12">
      <div className="ibox float-e-margins">
        <div className="ibox-content">
          <div className="row">
            <div {...getRootProps()} className="dropzone col-md-12">
              <input {...getInputProps()} />
              {isDragActive ? (
                <p>Solte suas fotos...</p>
              ) : (
                <p>Arraste suas fotos ou clique aqui para fazer o upload</p>
              )}
              {loading && <h1>Carregando ...</h1>}
            </div>
          </div>
          <div className="row">
            <p>
              {count} image{count != 1 ? "ns" : "m"}
            </p>
            {photos.map((photo, key) => (
              <Photo
                photo={photo}
                key={key}
                removePhoto={() => removePhoto(key)}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
