import React from "react";

import { format } from "date-fns";
import Currency from "../Currency";

export default function LocationContent({ data }) {
  return data ? (
    <div>
      <h3>
        Seu código de reserva é <i>{data.reservation_code}</i>
      </h3>

      <div className="col-md-6 col-md-offset-3 search-work">
        <div className="title-search-home">RESUMO</div>
        <div className="form-search-work">
          <div className="row text-left">
            <div className="col-md-12">
              <b>Data/Hora Selecionada</b>
            </div>
            <div className="col-md-12">
              <i className="fa fa-calendar"></i>
              &nbsp;De: {format(data.date_init, "DD/MM/YYYY")}-{data.hour_init}
            </div>
            <div className="col-md-12">
              <i className="fa fa-calendar"></i>
              &nbsp;Até: {format(data.date_end, "DD/MM/YYYY")}-{data.hour_end}
            </div>
            <hr className="col-md-12"></hr>
            <div className="col-md-6">
              <b>Preço da locação</b>

              <h2>
                <Currency number={data.price_total}></Currency>
              </h2>
              <br />
              <br />
            </div>
            <div className="col-md-6 text-right">
              <br />
              {data && data.payment_status === "waiting" && (
                <a
                  className="button "
                  href={`/finalizar-pagamento?advertising_location=${data.id}`}
                >
                  Finalizar e pagar
                </a>
              )}
            </div>

            <div className="col-md-12">
              {data.payment_status === "waiting" && (
                <a className="btn btn-danger">Cancelar reserva</a>
              )}
            </div>
          </div>
          <br />
          <br />
          <br />
          <b>
            Acompanhe sua reserva através de sua conta Oneer, <br />
            clicando no botão abaixo.
          </b>
          <br />
          <br />
          <a className="button btn-danger" href="/minhas-reservas">
            Minhas reservas
          </a>
        </div>
      </div>
    </div>
  ) : (
    <></>
  );
}
