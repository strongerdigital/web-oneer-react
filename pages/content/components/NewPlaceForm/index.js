import React, { useEffect, useState, useCallback } from "react";
import { getCep, uploadPhoto } from "../../../../providers/services/app";
import MaskedInput from "react-text-mask";
import { phoneMask, moneyMask, cepMask } from "../../../../consts/masks";
import AvailabilityForm from "../AvailabilityForm";
import { savePlace } from "../../../../providers/services/user";

import UploadPhotos from "../UploadPhotos";
import { store } from "../../../index";
import { setAppLoading } from "../../../../store/actions/app";
// import { Container } from './styles';

export default function NewPlaceForm({ data, logged }) {
  const [formData, setFormData] = useState({
    accessories: [],
    availabilities: []
  });
  const [first_document, setFirstDocument] = useState(null);
  const [second_document, setSecondDocument] = useState(null);
  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    setFormData({
      ...formData,
      user_id: logged.id
    });
  }, [logged]);

  const handleSubmit = async e => {
    e.preventDefault();
    var form = new FormData();
    for (var field in formData) {
      if (field === "availabilities") {
        form.append(field, JSON.stringify(formData[field]));
      } else if (field === "accessories") {
        form.append(field, JSON.stringify(formData[field]));
      } else {
        form.append(field, formData[field]);
        // form.append(field, formData[field]);
      }
    }
    if (first_document) {
      form.append("first_document", first_document, first_document.name);
    }
    if (second_document) {
      form.append("second_document", second_document, second_document.name);
    }
    const request = await savePlace(form);
    if (request.id) {
      if (photos) {
        if (photos && photos.length > 0) {
          store.dispatch(setAppLoading(true));
          for (const [idx, element] of photos.entries()) {
            var targetPath = element.file[0];

            var fileName = element.file[0].name;

            const uploadData = new FormData();
            uploadData.append("photo", targetPath, fileName);
            await uploadPhoto(uploadData, request.id);
          }
          store.dispatch(setAppLoading(false));
          location.href = `meuanuncio?id=${request.id}`;
        } else {
          location.href = `meuanuncio?id=${request.id}`;
        }
      }
    }
  };
  const handleCep = async () => {
    if (formData.zipcode && formData.zipcode.length === 9) {
      const response = await getCep(formData.zipcode);
      if (response.cidade) {
        setFormData({
          ...formData,
          neighborhood: response.bairro,
          city: response.cidade,
          city_id: response.city_id,
          street: response.logradouro,
          state: response.uf
        });
      }
    }
  };
  const addAvailability = () => {
    const avs = formData.availabilities || [];
    avs.push({});
    setFormData({
      ...formData,
      availabilities: avs
    });
  };
  const removeAvailability = key => {
    formData.availabilities.splice(key, 1);

    setFormData({
      ...formData,
      availabilities: formData.availabilities
    });
  };
  const setAvailabilitiyData = (key, itemData) => {
    formData.availabilities[key] = itemData;
    setFormData({
      ...formData,
      availabilities: formData.availabilities
    });
  };
  const handleAccessories = id => {
    const found = formData.accessories.includes(id);

    if (found) {
      const index = formData.accessories.indexOf(id);
      formData.accessories.splice(index, 1);
    } else {
      formData.accessories.push(id);
    }
    setFormData({
      ...formData,
      accessories: formData.accessories
    });
  };

  const setPDF1 = useCallback(acceptedFiles => {
    const file = acceptedFiles[0];

    var reader = new FileReader();
    reader.onloadend = function() {
      setFirstDocument(file);
    };
    reader.readAsDataURL(file);
  }, []);

  const handlePhotos = photos => {
    setPhotos(photos);
  };

  const setPDF2 = useCallback(acceptedFiles => {
    const file = acceptedFiles[0];

    var reader = new FileReader();
    reader.onloadend = function() {
      setSecondDocument(file);
    };
    reader.readAsDataURL(file);
  }, []);

  return (
    <form onSubmit={handleSubmit}>
      <div className="row">
        <div className="col-md-12">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Titulo
            </label>
            <input
              className="string required form-control field-contact"
              autoFocus="autofocus"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.title || ""}
              onChange={e => {
                setFormData({ ...formData, title: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-12">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Descrição
            </label>
            <textarea
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.description || ""}
              onChange={e => {
                setFormData({ ...formData, description: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Telefone
            </label>
            <MaskedInput
              guide={false}
              mask={phoneMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.phone || ""}
              onChange={e => {
                setFormData({ ...formData, phone: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Telefone II
            </label>
            <MaskedInput
              guide={false}
              mask={phoneMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.phone_second || ""}
              onChange={e => {
                setFormData({ ...formData, phone_second: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Preço por hora
            </label>
            <MaskedInput
              guide={false}
              mask={moneyMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.price || ""}
              onChange={e => {
                setFormData({ ...formData, price: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Ocupantes
            </label>
            <input
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="number"
              name="user[name]"
              id="user_name"
              value={formData.occupants || ""}
              onChange={e => {
                setFormData({ ...formData, occupants: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Categoria
            </label>
            <select
              className="string required form-control field-contact"
              value={formData.category_id || ""}
              onChange={e => {
                setFormData({ ...formData, category_id: e.target.value });
              }}
            >
              <option value="">Selecione</option>
              {data &&
                data.categories &&
                data.categories.map(item => (
                  <option key={item.id} value={item.id}>
                    {item.title}
                  </option>
                ))}
            </select>
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string optional user_name">
            <label className="string required" htmlFor="user_name">
              Tipo de anúncio
            </label>
            <select
              className="string optional form-control field-contact"
              value={formData.advertising_type || ""}
              onChange={e => {
                setFormData({ ...formData, advertising_type: e.target.value });
              }}
            >
              <option value="">Selecione</option>
              <option value="space">Espaço Comercial</option>
              <option value="event">Evento</option>
            </select>
          </div>
        </div>
        <div className="col-md-4">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Espaço para workpass?
            </label>
            <input
              className="string required form-control field-contact"
              type="checkbox"
              name="user[name]"
              id="user_name"
              value={formData.workpass || ""}
              onChange={e => {
                setFormData({ ...formData, workpass: e.target.value });
              }}
            />
          </div>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <h3>DADOS DE LOCALIZAÇÃO</h3>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Cep
            </label>
            <MaskedInput
              guide={false}
              mask={cepMask}
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              onBlur={handleCep}
              value={formData.zipcode || ""}
              onChange={e => {
                setFormData({ ...formData, zipcode: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-7">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Endereço
            </label>
            <input
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.street || ""}
              onChange={e => {
                setFormData({ ...formData, street: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-2">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Número
            </label>
            <input
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="number"
              name="user[name]"
              id="user_name"
              value={formData.number || ""}
              onChange={e => {
                setFormData({ ...formData, number: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Bairro
            </label>
            <input
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.neighborhood || ""}
              onChange={e => {
                setFormData({ ...formData, neighborhood: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-4">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> Cidade
            </label>
            <input
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              readOnly
              id="user_name"
              value={formData.city || ""}
              onChange={e => {
                setFormData({ ...formData, city: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-2">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> UF
            </label>
            <input
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              readOnly
              id="user_name"
              value={formData.state || ""}
              onChange={e => {
                setFormData({ ...formData, state: e.target.value });
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          <div className="input string required user_name">
            <label className="string required" htmlFor="user_name">
              <abbr title="obrigatório">*</abbr> País
            </label>
            <select
              className="string required form-control field-contact"
              value={formData.country_id || ""}
              onChange={e => {
                setFormData({ ...formData, country_id: e.target.value });
              }}
            >
              <option value="">Selecione</option>
              {data &&
                data.countries &&
                data.countries.map(item => (
                  <option key={item.id} value={item.slug}>
                    {item.name}
                  </option>
                ))}
            </select>
          </div>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <h3>DOCUMENTOS DO IMÓVEL</h3>
          <p>
            Você deverá adicionar os documentos do imóvel para verificação do
            anúncio.
          </p>
          <div className="col-md-4">
            <div className="input string required user_name">
              <label className="string required" htmlFor="user_name">
                <abbr title="obrigatório">*</abbr> Documento 1
              </label>
              <input
                type="file"
                onChange={e => {
                  setPDF1(e.target.files);
                }}
                accept=".pdf, .doc, .docx, .xls, .xlsx"
              />
            </div>
          </div>
          <div className="col-md-4">
            <div className="input string required user_name">
              <label className="string required" htmlFor="user_name">
                <abbr title="obrigatório">*</abbr> Documento 2
              </label>
              <input
                type="file"
                onChange={e => {
                  setPDF2(e.target.files);
                }}
                accept=".pdf, .png, .jpg, .jpeg"
              />
            </div>
          </div>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <h3>DISPONIBILIDADE</h3>
        </div>
        <div className="col-md-12">
          {formData.availabilities &&
            formData.availabilities.map((item, key) => (
              <AvailabilityForm
                key={key}
                item={item}
                setAvailabilitiyData={itemData => {
                  setAvailabilitiyData(key, itemData);
                }}
                removeAvailability={() => {
                  removeAvailability(key);
                }}
              ></AvailabilityForm>
            ))}
          <a onClick={addAvailability}>Adicionar</a>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <h3>QUANTIDADE DE ESPAÇOS DISPONÍVEIS</h3>
          <br />
        </div>
        <div className="col-md-4">
          <div className="input string required user_name">
            <input
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="number"
              name="user[name]"
              id="user_name"
              value={formData.quantity_spaces || ""}
              onChange={e => {
                setFormData({ ...formData, quantity_spaces: e.target.value });
              }}
            />
          </div>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <h3>DADOS ADICIONAIS</h3>
        </div>
        <div className="col-md-12">
          <div className="input string required user_name">
            <textarea
              className="string required form-control field-contact"
              required="required"
              aria-required="true"
              type="text"
              name="user[name]"
              id="user_name"
              value={formData.observation || ""}
              onChange={e => {
                setFormData({ ...formData, observation: e.target.value });
              }}
            />
          </div>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <h3>COMODIDADES E ACESSIBILIDADE</h3>
        </div>
        <div className="col-md-12">
          <div className="relative">
            <div className="checkboxes  margin-bottom-10">
              {data &&
                data.accessories &&
                data.accessories.map(item => (
                  <div key={item.id} className="col-md-4">
                    <input
                      onChange={() => {
                        handleAccessories(item.id);
                      }}
                      id={`check-${item.id}`}
                      type="checkbox"
                      name="accessories"
                      value={item.id}
                    />
                    <label htmlFor={`check-${item.id}`}>{item.name}</label>
                    <br />
                  </div>
                ))}
            </div>
          </div>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <h3>GALERIA DE FOTOS</h3>
          <UploadPhotos handlePhotos={handlePhotos}></UploadPhotos>
        </div>
        <hr className="col-md-12"></hr>
        <div className="col-md-12">
          <button className="button fullwidth margin-top-30" type="submit">
            Confirmar
          </button>
          <br />
          <br />
        </div>
      </div>
    </form>
  );
}
