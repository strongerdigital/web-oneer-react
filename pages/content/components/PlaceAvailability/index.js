import React from "react";
import Currency from "../Currency";

// import { Container } from './styles';

export default function PlaceAvailability({
  availability,
  makeReservation,
  loggedUser
}) {
  return (
    <div className="panel panel-default panel-accessories-show">
      <div className="panel-body">
        <h3>Reserva Disponível!</h3>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12">
            <div className="alert alert-info">
              <div className="minspace"></div>
              <span className="text-block">
                <i className="icon icon-calendar"></i>
                <span className="icon-ml">
                  de <strong>{availability && availability.date_init}</strong>
                  <strong>{availability && availability.hour_init}</strong>
                </span>
              </span>
              <br />
              <span className="text-block icon-tp">
                <i className="icon icon-calendar"></i>
                <span className="icon-ml">
                  até <strong>{availability && availability.date_end}</strong>
                  <strong>{availability && availability.hour_end}</strong>
                </span>
              </span>
            </div>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-12">
            <div className="alert alert-info text-right">
              <div className="text-center">
                <h4>Valor da Locação:</h4>
                <h3 className="text-success">
                  <Currency
                    number={availability && availability.price_total}
                  ></Currency>
                </h3>
              </div>
            </div>
          </div>
        </div>
        {loggedUser && loggedUser.id && (
          <input
            onClick={makeReservation}
            type="submit"
            value="Reservar Espaço Comercial"
            className="btn btn-success btn-featured btn-block"
            data-disable-with="Reservar Espaço Comercial"
          />
        )}
        {loggedUser && !loggedUser.id && (
          <a
            href="/minhaconta"
            className="btn btn-success btn-featured btn-block"
          >
            Faça login para poder continuar
          </a>
        )}
        <div className="gap"></div>
        <br />
        <br />
        <br />
      </div>
    </div>
  );
}
