import React from "react";
import CreditCard from "../CreditCard";
import { makePayment, getLogged } from "../../../../providers/services/user";

// import { Container } from './styles';

export default function MakePaymentContent({ data }) {
  const handleMakePayment = infos => {
    getLogged().then(user => {
      data &&
        makePayment(
          {
            ...infos,
            advertising_location: data.id
          },
          user
        ).then(response => {
          location.href = "location?advertising_location=" + data.id;
        });
    });
  };
  return (
    <div className="row">
      <div className="col-md-12">
        <CreditCard handleMakePayment={handleMakePayment}></CreditCard>
        <img src="/static/images/bandeiras-cartao.png" />
        <br />
        <small>*Cartões de Crédito aceitos</small>
        <br />
        <br />
        <br />
      </div>
    </div>
  );
}
