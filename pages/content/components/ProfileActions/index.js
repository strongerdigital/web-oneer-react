import React from "react";

// import { Container } from './styles';

export default function ProfileActions() {
  return (
    <div className="navmenu-box">
      <div className="row">
        <div className="col-xs-12 col-sm-4 col-md-4"></div>
        <div className="col-xs-12 col-sm-8 col-md-8">
          <div className="text-right">
            <ul className="list-inline">
              <li>
                <a
                  title="Reservas dos Anúncios"
                  className="btn btn-warning btn-featured btn-mr"
                  href="/reservas"
                >
                  Reservas dos Anúncios
                </a>
              </li>
              <li>
                <a
                  title="Visualizar Minhas Mensagens"
                  className="btn btn-primary btn-featured btn-mr"
                  href="/minhas-mensagens"
                >
                  Mensagens
                </a>
              </li>
              <li>
                <a
                  title="Anunciar novo espaço comercial para locação"
                  className="btn btn-success btn-featured"
                  href="/novoanuncio"
                >
                  <i className="icon icon-plus icon-md icon-mr"></i>Novo Anúncio
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
