import React, { useState } from "react";

// import { Container } from './styles';

export default function SerchSimple() {
  const [search, setSearch] = useState({});
  return (
    <div className="container">
      <div className="main-search-input margin-bottom-35">
        <input
          type="text"
          className="ico-01"
          placeholder="Nome da rua, cidade, coworking..."
          value={search.term || ""}
          onChange={e => {
            setSearch({
              term: e.target.value
            });
          }}
        />
        <button className="button">Pesquise...</button>
      </div>
    </div>
  );
}
