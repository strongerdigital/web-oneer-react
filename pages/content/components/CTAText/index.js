import React from "react";

// import { Container } from './styles';

export default function CTAText() {
  return (
    <div className="container">
      <div className="row margin-bottom-25 margin-top-65">
        <div className="col-md-6">
          <h3 className="headline margin-bottom-25 ">Reserve pela Oneer</h3>
          <p>
            Trabalhe com conveniência pelas cidades. Curta sua festa com o que
            há de melhor.
          </p>

          <p>
            Compare preços e comodidades. <b>Dê adeus à burocracia. </b>
          </p>
          <p>
            Existe uma nova forma de interagir com clientes, pacientes, alunos e
            parceiros: a ONEER.
          </p>

          <p>
            Nossa forma de reservar é prática, moderna e permite que você
            compare o que há de mais usual e arrojado.
          </p>

          <p>
            Ah, e é claro, pensamos em <b>economizar o seu dinheiro. </b>
          </p>
        </div>
        <div className="col-md-6">
          <img src="/static/images/about.png" className="img-responsive" />
        </div>
      </div>
    </div>
  );
}
