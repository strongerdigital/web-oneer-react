import React from "react";
import Title from "./components/Title";
import Results from "./components/Results";
import Filter from "./components/Filter";

const CategoryPrincipal = ({ data }) => (
  <>
    <div className="clearfix"></div>
    {data && (
      <Title title={`Categoria ${data.title ? "| " + data.title : ""}`}></Title>
    )}
    <div className="container">
      <div className="row sticky-wrapper">
        {data && <Results data={data.places}></Results>}
        {data && data.places && data.places.length > 0 && (
          <Filter data={data} nocategory></Filter>
        )}
      </div>
    </div>
  </>
);

export default CategoryPrincipal;
