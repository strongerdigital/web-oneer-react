import React, { useEffect, useState } from "react";
import Title from "./components/Title";
import { connect } from "react-redux";
import { getLogged } from "../../providers/services/user";
import * as AppActions from "../../store/actions/app";
import LocationContent from "./components/LocationContent";
// import { Container } from './styles';

function LocationPrincipal({ data }) {
  const [logged, setLogged] = useState(false);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        setLogged(user);
      }
    });
  }, []);
  return (
    <>
      <div className="clearfix"></div>
      <Title
        title={"Confirmação de Reserva"}
        breadcrumbs={`Olá ${logged &&
          logged.name}, sua reserva foi realizada com sucesso.`}
      ></Title>
      <div className="container">
        <div className="row sticky-wrapper">
          <div className="row">
            <div className="col-md-12 text-center">
              {data && data.reservation_code && (
                <LocationContent data={data}></LocationContent>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default LocationPrincipal;
