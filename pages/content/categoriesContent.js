import React from "react";
import Categories from "./components/Categories";
import Title from "./components/Title";

const CategoriesPrincipal = ({ data }) => (
  <>
    <div className="clearfix"></div>
    <Title title={"Categorias"}></Title>
    <Categories data={data}></Categories>
  </>
);

export default CategoriesPrincipal;
