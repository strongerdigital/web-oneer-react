import React, { useEffect } from "react";
import Title from "./components/Title";
import ProfileActions from "./components/ProfileActions";
import ProfileMenu from "./components/ProfileMenu";
import ProfileContent from "./components/ProfileContent";
// import { Container } from './styles';

function MyPlacePrincipal() {
  return (
    <>
      <div className="clearfix"></div>
      <Title title={"Meu espaço"}></Title>
      <div className="container">
        <div className="row sticky-wrapper">
          <ProfileActions></ProfileActions>
          <div className="row">
            <ProfileMenu></ProfileMenu>
            <div className="col-md-9">
              <ProfileContent></ProfileContent>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default MyPlacePrincipal;
