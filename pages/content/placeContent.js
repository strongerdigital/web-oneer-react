import React from "react";
import Title from "./components/Title";
import Photos from "./components/Photos";
import PlaceDescription from "./components/PlaceDescription";
import PlaceSidebar from "./components/PlaceSidebar";
import PlaceMap from "./components/PlaceMap";

// import { Container } from './styles';

export default function PlacePrincipal({ data }) {
  const place = data ? data.place : {};
  return (
    <>
      <div className="clearfix"></div>
      <Title title={place.title} breadcrumbs={place.street}></Title>
      <Photos place={place}></Photos>
      <div className="container">
        <div className="row">
          <PlaceDescription place={place}></PlaceDescription>
          <PlaceSidebar place={place}></PlaceSidebar>
        </div>
      </div>
      <PlaceMap place={place}></PlaceMap>
    </>
  );
}
