import React from "react";
import Title from "./components/Title";

const AboutPrincipal = props => (
  <>
    <div className="clearfix"></div>
    <Title title={"Quem Somos - Oneer"}></Title>
    <div className="container">
      <div className="space"></div>
      <div className="text-center">
        <p>
          Somos uma empresa que irá revolucionar a forma de locação de espaços
          para trabalho e eventos, <br />
          com a flexibilidade e segurança que você precisa, sem burocracia e
          equipado com as facilidades necessárias.
        </p>
        <p>
          Pensando na mobilidade das cidades e das pessoas, viemos para
          facilitar a vida dos que trabalham e <br />
          buscam otimizar seu tempo e seu dinheiro, permitindo a reserva
          temporária e livre de papelada, <br />
          seja para trabalhar ou curtir o happy hour com os amigos.{" "}
        </p>
        <p>
          Viabilizamos sonhos e projetos de quem veio ao mundo para brilhar,
          seja no escritório ou na pista.{" "}
        </p>
        <p>
          Para o recém-formado, o aniversariante, os noivos, o profissional
          experiente, para todos.{" "}
        </p>
        <p>
          Somos a <strong>Oneer</strong>, a startup que conecta o espaço com o
          usuário de uma forma rápida e segura.
        </p>
      </div>
      <div className="space"></div>
      <div className="alert alert-warning">
        <video
          controls="controls"
          poster="/capa-video-oneer.png"
          width=" 100%"
          type="video/mp4"
          src="/video-institucional-oneer.mp4"
        ></video>
      </div>
      <div className="space"></div>
    </div>
  </>
);

export default AboutPrincipal;
