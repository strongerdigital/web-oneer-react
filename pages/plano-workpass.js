import React from "react";
import Main from "./layouts/main";

const Workpass = ({ data }) => <Main data={data} />;

Workpass.getInitialProps = async () => {
  return {
    data: {
      page: "workpass"
    }
  };
};

export default Workpass;
