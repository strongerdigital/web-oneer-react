import React, { Component } from "react";
import Head from "next/head";

const Styles = props => (
  <>
    <Head>
      <link
        rel="shortcut icon"
        href="/static/favicon.ico"
        type="image/x-icon"
      />
      <link rel="icon" href="/static/favicon.ico" type="image/x-icon"></link>
      <title>Aluguel de escritorios e espaços comerciais | Oneer</title>
      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1"
      />
      <link rel="stylesheet" href="/static/css/style.css" />
      <link rel="stylesheet" href="/static/css/colors/main.css" id="colors" />
      <link rel="stylesheet" href="/static/css/datepicker.css" id="colors" />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css"
      />
    </Head>
  </>
);

export default Styles;
