import React, { useState } from "react";

function Loading() {
  const [loading, setLoading] = useState(false);
  return loading ? (
    <div className="loader">
      <i className="fa fa-spin fa-spinner"></i>
    </div>
  ) : (
    <></>
  );
}

export default Loading;
