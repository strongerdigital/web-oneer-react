import React, { useEffect, useState } from "react";
import { getLogged } from "../../providers/services/user";

function Header() {
  const [logged, setLogged] = useState(false);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        setLogged(user);
      }
    });
  }, []);
  return (
    <>
      <header id="header-container">
        <div id="header">
          <div className="container">
            <div className="left-side">
              <div id="logo">
                <a href="/">
                  <img src="/static/images/logo-oneer.png" alt="" />
                </a>
              </div>

              <div className="mmenu-trigger">
                <button className="hamburger hamburger--collapse" type="button">
                  <span className="hamburger-box">
                    <span className="hamburger-inner"></span>
                  </span>
                </button>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="right-side">
              <nav id="navigation" className="style-1">
                <ul id="responsive">
                  <li>
                    <a href="/">Home</a>
                  </li>
                  <li>
                    <a href={logged ? "/novoanuncio" : "/minhaconta"}>
                      Divulgue seu espaço
                    </a>
                  </li>
                  <li>
                    <a href="/espacos">Espaços</a>
                  </li>
                  <li>
                    <a href="/categorias">Categorias</a>
                  </li>
                  <li className="login-oneer">
                    <a href={logged ? "/profile" : "/minhaconta"}>
                      {"Minha conta"}
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>
      <div className="clearfix"></div>
    </>
  );
}

export default Header;
