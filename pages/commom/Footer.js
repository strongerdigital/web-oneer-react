import React, { useEffect, useState } from "react";
import Scripts from "./Scripts";
import { getLogged } from "../../providers/services/user";
function Footer() {
  const [logged, setLogged] = useState(false);
  useEffect(() => {
    getLogged().then(user => {
      if (user && user.id) {
        setLogged(user);
      }
    });
  }, []);
  return (
    <>
      <div className="clearfix"></div>
      <div id="footer" className="sticky-footer">
        <div className="container">
          <div className="row">
            <div className="col-md-3 col-sm-6">
              <img
                className="footer-logo"
                src="/static/images/logo-oneer-white.png"
                alt=""
              />
            </div>

            <div className="col-md-3 col-sm-6 ">
              <h4>Navegação</h4>

              <ul className="footer-links">
                <li>
                  <a href="/">Início</a>
                </li>
                <li>
                  <a href="/quem-somos">Quem Somos</a>
                </li>

                <li>
                  <a href="/categorias">Categorias</a>
                </li>
                <li>
                  <a href="/termos-de-uso">Termos de Uso</a>
                </li>
                <li>
                  <a href="/termos-de-uso">Políticas de Privacidade</a>
                </li>
                <li>
                  <a href={logged ? "/profile" : "/minhaconta"}>
                    {logged ? "Minha Conta" : "Fazer login"}
                  </a>
                </li>
              </ul>

              <div className="clearfix"></div>
            </div>

            <div className="col-md-3  col-sm-12">
              <h4>Suporte</h4>
              <ul className="footer-links">
                <li>
                  <a href="#">Central de Ajuda</a>
                </li>
                <li>
                  <a href="#">Entre em Contato</a>
                </li>
              </ul>
            </div>
            <div className="col-md-3  col-sm-12">
              <h4>Redes sociais</h4>
              <ul className="social-icons margin-top-20">
                <li>
                  <a className="facebook" href="#">
                    <i className="icon-facebook"></i>
                  </a>
                </li>
                <li>
                  <a className="twitter" href="#">
                    <i className="icon-twitter"></i>
                  </a>
                </li>
                <li>
                  <a className="gplus" href="#">
                    <i className="icon-gplus"></i>
                  </a>
                </li>
                <li>
                  <a className="vimeo" href="#">
                    <i className="icon-vimeo"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="copyrights">
                © 2019 Oneer. Todos os direitos reservados.
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="backtotop">
        <a href="#"></a>
      </div>
      <Scripts></Scripts>
    </>
  );
}

export default Footer;
