import React, { Component } from "react";

export default class Scripts extends Component {
  render() {
    return (
      <>
        <script
          type="text/javascript"
          src="/static/scripts/jquery-2.2.0.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/chosen.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/bootstrap.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/magnific-popup.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/owl.carousel.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/rangeSlider.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/sticky-kit.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/slick.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/masonry.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/mmenu.min.js"
        ></script>
        <script
          type="text/javascript"
          src="/static/scripts/tooltips.min.js"
        ></script>
      </>
    );
  }
}
