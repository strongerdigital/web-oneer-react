import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const MakePayment = ({ data }) => <Main data={data} />;

MakePayment.getInitialProps = async ({ query }) => {
  return {
    data: {
      location: await Loader.getLocation(query.advertising_location),
      page: "makepayment"
    }
  };
};

export default MakePayment;
