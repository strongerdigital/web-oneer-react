import React from "react";

//pages
import Index from "../content/indexContent";
import Places from "../content/placesContent";
import Place from "../content/placeContent";
import Categories from "../content/categoriesContent";
import Category from "../content/categoryContent";
import TermsOfUse from "../content/termsOfUseContent";
import About from "../content/AboutContent";
import Profile from "../content/ProfileContent";
import UpdateProfile from "../content/UpdateProfileContent";
import NewPlace from "../content/NewPlaceContent";
import MyPlace from "../content/MyPlaceContent";
import MyPlaces from "../content/MyPlacesContent";
import Location from "../content/LocationContent";
import MyLocations from "../content/MyLocationsContent";
import MakePayment from "../content/MakePaymentContent";

//components
import Styles from "../commom/Styles";
import Header from "../commom/Header";
import Footer from "../commom/Footer";
import Loading from "../commom/Loading";
import MyAccount from "../content/MyAccountContent";
import SignUp from "../content/components/Auth/SignUp";

const Main = ({ data }) => (
  <React.Fragment>
    <Loading />
    <Styles />

    {data && (
      <div id="wrapper">
        <Header />
        {
          {
            index: <Index data={data} />,
            places: <Places data={data} />,
            place: <Place data={data} />,
            categories: <Categories data={data && data.categories} />,
            category: <Category data={data && data.category} />,
            termsOfUse: <TermsOfUse />,
            about: <About />,
            myAccount: <MyAccount />,
            signUp: <SignUp />,
            profile: <Profile />,
            updateProfile: <UpdateProfile />,
            newPlace: <NewPlace data={data} />,
            myplace: <MyPlace data={data}></MyPlace>,
            myplaces: <MyPlaces data={data}></MyPlaces>,
            location: <Location data={data && data.location}></Location>,
            myLocations: <MyLocations></MyLocations>,
            makepayment: (
              <MakePayment data={data && data.location}></MakePayment>
            ),
            myFavorites: <Profile />,
            workpass: <Profile />,
            locations: <Profile />,
            messages: <Profile />
          }[data.page]
        }
      </div>
    )}
    <Footer />
  </React.Fragment>
);

export default Main;
