import React from "react";
import Main from "./layouts/main";

const MyAccount = ({ data }) => <Main data={data} />;

MyAccount.getInitialProps = async () => {
  return {
    data: {
      page: "myAccount"
    }
  };
};

export default MyAccount;
