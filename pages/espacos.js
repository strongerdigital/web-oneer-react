import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const Places = ({ data }) => <Main data={data} />;

Places.getInitialProps = async ({ query }) => {
  return {
    data: {
      places: query.busca
        ? await Loader.search(query.busca)
        : await Loader.featured(),
      categories: await Loader.categories(),
      accessories: await Loader.accessories(),
      page: "places"
    }
  };
};

export default Places;
