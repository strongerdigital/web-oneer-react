import React from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

const MyPlaces = ({ data }) => <Main data={data} />;

MyPlaces.getInitialProps = async () => {
  return {
    data: {
      page: "myplaces"
    }
  };
};

export default MyPlaces;
