import React from "react";
import Main from "./layouts/main";

const TermsOfUse = ({ data }) => <Main data={data} />;

TermsOfUse.getInitialProps = async () => {
  return {
    data: {
      page: "termsOfUse"
    }
  };
};

export default TermsOfUse;
