import React from "react";
import Main from "./layouts/main";

const Profile = ({ data }) => <Main data={data} />;

Profile.getInitialProps = async () => {
  return {
    data: {
      page: "profile"
    }
  };
};

export default Profile;
