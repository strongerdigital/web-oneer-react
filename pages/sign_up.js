import React from "react";
import Main from "./layouts/main";

const SignUp = ({ data }) => <Main data={data} />;

SignUp.getInitialProps = async () => {
  return {
    data: {
      page: "signUp"
    }
  };
};

export default SignUp;
