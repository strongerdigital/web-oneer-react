import React, { useEffect, useState } from "react";
import Main from "./layouts/main";
import Loader from "../providers/services/places";

import reducers from "../store/reducers";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
export const store = createStore(reducers, applyMiddleware(thunk));

export default function pages() {
  const [data, setData] = useState({});

  const load = async () => {
    const item = {
      categories: await Loader.categories(),
      last4: await Loader.last4(),
      page: "index"
    };
    setData(item);
  };

  useEffect(() => {
    load();
  }, []);
  return <Provider store={store}>{data.page && <Main data={data} />}</Provider>;
}
